﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Forms
{
    public partial class frmAtalhos : Form
    {
        public frmAtalhos()
        {
            InitializeComponent();
        }
        #region Atalhos
        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Usuario";
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Relatorios";
        }

        private void btnLivros_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Livros";
        }

        private void btnDevolução_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Devolução";
        }
        

        private void btnAcervo_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Acervo";
        }

        private void lblClose_MouseHover(object sender, EventArgs e)
        {
            lblatalho.Text = "Fechar";
        }
        #endregion

    }
}
