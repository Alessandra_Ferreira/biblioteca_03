﻿namespace Biblioteca.Forms
{
    partial class frmAtalhos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAtalhos));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblClose = new System.Windows.Forms.PictureBox();
            this.btnLivros = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnAcervo = new System.Windows.Forms.PictureBox();
            this.btnEmprestimo = new System.Windows.Forms.PictureBox();
            this.btnDevolução = new System.Windows.Forms.PictureBox();
            this.lblatalho = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLivros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAcervo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEmprestimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDevolução)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 20;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(70)))), ((int)(((byte)(102)))));
            this.panel1.Location = new System.Drawing.Point(-1, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(296, 16);
            this.panel1.TabIndex = 0;
            // 
            // lblClose
            // 
            this.lblClose.Image = ((System.Drawing.Image)(resources.GetObject("lblClose.Image")));
            this.lblClose.Location = new System.Drawing.Point(223, -5);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(42, 40);
            this.lblClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.lblClose.TabIndex = 1;
            this.lblClose.TabStop = false;
            this.lblClose.MouseHover += new System.EventHandler(this.lblClose_MouseHover);
            // 
            // btnLivros
            // 
            this.btnLivros.Image = ((System.Drawing.Image)(resources.GetObject("btnLivros.Image")));
            this.btnLivros.Location = new System.Drawing.Point(93, 54);
            this.btnLivros.Name = "btnLivros";
            this.btnLivros.Size = new System.Drawing.Size(41, 39);
            this.btnLivros.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnLivros.TabIndex = 2;
            this.btnLivros.TabStop = false;
            this.btnLivros.MouseHover += new System.EventHandler(this.btnLivros_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(46, 89);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(41, 39);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(-1, 54);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(41, 39);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // btnAcervo
            // 
            this.btnAcervo.Image = ((System.Drawing.Image)(resources.GetObject("btnAcervo.Image")));
            this.btnAcervo.Location = new System.Drawing.Point(224, 89);
            this.btnAcervo.Name = "btnAcervo";
            this.btnAcervo.Size = new System.Drawing.Size(41, 39);
            this.btnAcervo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAcervo.TabIndex = 5;
            this.btnAcervo.TabStop = false;
            this.btnAcervo.MouseHover += new System.EventHandler(this.btnAcervo_MouseHover);
            // 
            // btnEmprestimo
            // 
            this.btnEmprestimo.Image = ((System.Drawing.Image)(resources.GetObject("btnEmprestimo.Image")));
            this.btnEmprestimo.Location = new System.Drawing.Point(177, 54);
            this.btnEmprestimo.Name = "btnEmprestimo";
            this.btnEmprestimo.Size = new System.Drawing.Size(41, 39);
            this.btnEmprestimo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEmprestimo.TabIndex = 6;
            this.btnEmprestimo.TabStop = false;
            // 
            // btnDevolução
            // 
            this.btnDevolução.Image = ((System.Drawing.Image)(resources.GetObject("btnDevolução.Image")));
            this.btnDevolução.Location = new System.Drawing.Point(140, 89);
            this.btnDevolução.Name = "btnDevolução";
            this.btnDevolução.Size = new System.Drawing.Size(41, 39);
            this.btnDevolução.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnDevolução.TabIndex = 7;
            this.btnDevolução.TabStop = false;
            this.btnDevolução.MouseHover += new System.EventHandler(this.btnDevolução_MouseHover);
            // 
            // lblatalho
            // 
            this.lblatalho.AutoSize = true;
            this.lblatalho.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblatalho.Location = new System.Drawing.Point(102, 7);
            this.lblatalho.Name = "lblatalho";
            this.lblatalho.Size = new System.Drawing.Size(68, 25);
            this.lblatalho.TabIndex = 8;
            this.lblatalho.Text = "Atalhos";
            // 
            // frmAtalhos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(275, 137);
            this.Controls.Add(this.lblatalho);
            this.Controls.Add(this.btnDevolução);
            this.Controls.Add(this.btnEmprestimo);
            this.Controls.Add(this.btnAcervo);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnLivros);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAtalhos";
            this.Text = "frmOutroMenu";
            ((System.ComponentModel.ISupportInitialize)(this.lblClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLivros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAcervo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEmprestimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDevolução)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox lblClose;
        private System.Windows.Forms.PictureBox btnDevolução;
        private System.Windows.Forms.PictureBox btnEmprestimo;
        private System.Windows.Forms.PictureBox btnAcervo;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox btnLivros;
        private MetroFramework.Controls.MetroLabel lblatalho;
    }
}