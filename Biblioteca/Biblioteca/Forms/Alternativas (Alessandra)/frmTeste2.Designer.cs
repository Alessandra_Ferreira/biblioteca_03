﻿namespace Biblioteca.Forms
{
    partial class frmTeste2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTeste2));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.lblClose = new System.Windows.Forms.PictureBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.lblClose)).BeginInit();
            this.panel34.SuspendLayout();
            this.metroTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(103, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 450);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel2.Location = new System.Drawing.Point(1, -3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(52, 36);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel3.Location = new System.Drawing.Point(28, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(52, 36);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel4.Location = new System.Drawing.Point(52, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(52, 36);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel5.Location = new System.Drawing.Point(1, 33);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(52, 36);
            this.panel5.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel6.Location = new System.Drawing.Point(53, 33);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(52, 36);
            this.panel6.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel7.Location = new System.Drawing.Point(1, 105);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(52, 36);
            this.panel7.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel8.Location = new System.Drawing.Point(54, 105);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(52, 36);
            this.panel8.TabIndex = 6;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel9.Location = new System.Drawing.Point(1, 69);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(52, 36);
            this.panel9.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel10.Location = new System.Drawing.Point(38, 87);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(52, 36);
            this.panel10.TabIndex = 8;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel11.Location = new System.Drawing.Point(53, 69);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(52, 36);
            this.panel11.TabIndex = 9;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel12.Location = new System.Drawing.Point(54, 243);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(52, 36);
            this.panel12.TabIndex = 16;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel13.Location = new System.Drawing.Point(38, 225);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(52, 36);
            this.panel13.TabIndex = 18;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel14.Location = new System.Drawing.Point(1, 207);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(52, 36);
            this.panel14.TabIndex = 15;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel15.Location = new System.Drawing.Point(1, 243);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(52, 36);
            this.panel15.TabIndex = 17;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel16.Location = new System.Drawing.Point(1, 171);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(52, 36);
            this.panel16.TabIndex = 12;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel17.Location = new System.Drawing.Point(53, 207);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(52, 36);
            this.panel17.TabIndex = 19;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel18.Location = new System.Drawing.Point(28, 154);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(52, 36);
            this.panel18.TabIndex = 11;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel19.Location = new System.Drawing.Point(1, 135);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(52, 36);
            this.panel19.TabIndex = 10;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel20.Location = new System.Drawing.Point(52, 140);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(52, 36);
            this.panel20.TabIndex = 13;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel21.Location = new System.Drawing.Point(53, 171);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(52, 36);
            this.panel21.TabIndex = 14;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel22.Location = new System.Drawing.Point(54, 375);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(52, 36);
            this.panel22.TabIndex = 26;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel23.Location = new System.Drawing.Point(38, 357);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(52, 36);
            this.panel23.TabIndex = 28;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel24.Location = new System.Drawing.Point(1, 339);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(52, 36);
            this.panel24.TabIndex = 25;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel25.Location = new System.Drawing.Point(1, 375);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(52, 36);
            this.panel25.TabIndex = 27;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(32)))), ((int)(((byte)(6)))));
            this.panel26.Location = new System.Drawing.Point(1, 303);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(52, 36);
            this.panel26.TabIndex = 22;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel27.Location = new System.Drawing.Point(53, 339);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(52, 36);
            this.panel27.TabIndex = 29;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(216)))), ((int)(((byte)(72)))));
            this.panel28.Location = new System.Drawing.Point(28, 286);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(52, 36);
            this.panel28.TabIndex = 21;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(134)))), ((int)(((byte)(244)))));
            this.panel29.Location = new System.Drawing.Point(1, 267);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(52, 36);
            this.panel29.TabIndex = 20;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(216)))), ((int)(((byte)(28)))));
            this.panel30.Location = new System.Drawing.Point(52, 272);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(52, 36);
            this.panel30.TabIndex = 23;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(14)))), ((int)(((byte)(88)))));
            this.panel31.Location = new System.Drawing.Point(53, 303);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(52, 36);
            this.panel31.TabIndex = 24;
            // 
            // lblClose
            // 
            this.lblClose.Image = ((System.Drawing.Image)(resources.GetObject("lblClose.Image")));
            this.lblClose.Location = new System.Drawing.Point(728, 2);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(42, 40);
            this.lblClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.lblClose.TabIndex = 33;
            this.lblClose.TabStop = false;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.metroTabControl1);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel34.Location = new System.Drawing.Point(115, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(653, 409);
            this.panel34.TabIndex = 34;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTabControl1.Location = new System.Drawing.Point(0, 0);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 2;
            this.metroTabControl1.Size = new System.Drawing.Size(653, 409);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.BackColor = System.Drawing.Color.Black;
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(645, 367);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "metroTabPage1";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(645, 367);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "metroTabPage2";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.BackColor = System.Drawing.Color.Black;
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(645, 367);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "metroTabPage3";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // frmTeste2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(768, 409);
            this.Controls.Add(this.panel34);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel27);
            this.Controls.Add(this.panel28);
            this.Controls.Add(this.panel29);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel31);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTeste2";
            this.Text = "frmTeste2";
            ((System.ComponentModel.ISupportInitialize)(this.lblClose)).EndInit();
            this.panel34.ResumeLayout(false);
            this.metroTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.PictureBox lblClose;
        private System.Windows.Forms.Panel panel34;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
    }
}