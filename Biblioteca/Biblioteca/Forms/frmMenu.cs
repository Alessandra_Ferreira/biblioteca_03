﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Forms
{
    public partial class frmMenu : Form
    {
        public static frmMenu Atual;
        public frmMenu()
        {
            InitializeComponent();
        }
        public void OpenScreen(UserControl control)
        {
            if (PanelPrincipal.Controls.Count == 1)
                PanelPrincipal.Controls.RemoveAt(0);

            PanelPrincipal.Controls.Add(control);
                    
        }

        private void btnexit_Click(object sender, EventArgs e)
        {

        }
    }
}
