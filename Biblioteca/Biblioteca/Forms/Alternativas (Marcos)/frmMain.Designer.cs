﻿namespace Biblioteca
{
    partial class frmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.tblMenu = new MetroFramework.Controls.MetroTabControl();
            this.tpgHome = new MetroFramework.Controls.MetroTabPage();
            this.tpgAcervos = new MetroFramework.Controls.MetroTabPage();
            this.tpgDevolucao = new MetroFramework.Controls.MetroTabPage();
            this.tpgEmprestimo = new MetroFramework.Controls.MetroTabPage();
            this.tpgRelatórios = new MetroFramework.Controls.MetroTabPage();
            this.ElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1.SuspendLayout();
            this.tblMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel1.Controls.Add(this.btnFechar);
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(930, 48);
            this.panel1.TabIndex = 0;
            // 
            // btnFechar
            // 
            this.btnFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFechar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.ForeColor = System.Drawing.Color.White;
            this.btnFechar.Location = new System.Drawing.Point(875, 0);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(55, 48);
            this.btnFechar.TabIndex = 1;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitulo.Font = new System.Drawing.Font("Comic Sans MS", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(153, 29);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "Biblioteca APP";
            // 
            // tblMenu
            // 
            this.tblMenu.Controls.Add(this.tpgHome);
            this.tblMenu.Controls.Add(this.tpgAcervos);
            this.tblMenu.Controls.Add(this.tpgDevolucao);
            this.tblMenu.Controls.Add(this.tpgEmprestimo);
            this.tblMenu.Controls.Add(this.tpgRelatórios);
            this.tblMenu.Cursor = System.Windows.Forms.Cursors.Default;
            this.tblMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMenu.Location = new System.Drawing.Point(0, 48);
            this.tblMenu.Name = "tblMenu";
            this.tblMenu.SelectedIndex = 0;
            this.tblMenu.Size = new System.Drawing.Size(930, 514);
            this.tblMenu.TabIndex = 3;
            this.tblMenu.UseSelectable = true;
            // 
            // tpgHome
            // 
            this.tpgHome.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpgHome.HorizontalScrollbarBarColor = true;
            this.tpgHome.HorizontalScrollbarHighlightOnWheel = false;
            this.tpgHome.HorizontalScrollbarSize = 10;
            this.tpgHome.Location = new System.Drawing.Point(4, 38);
            this.tpgHome.Name = "tpgHome";
            this.tpgHome.Size = new System.Drawing.Size(922, 472);
            this.tpgHome.TabIndex = 0;
            this.tpgHome.Text = "Home";
            this.tpgHome.VerticalScrollbarBarColor = true;
            this.tpgHome.VerticalScrollbarHighlightOnWheel = false;
            this.tpgHome.VerticalScrollbarSize = 10;
            this.tpgHome.Click += new System.EventHandler(this.tpgHome_Click);
            // 
            // tpgAcervos
            // 
            this.tpgAcervos.HorizontalScrollbarBarColor = true;
            this.tpgAcervos.HorizontalScrollbarHighlightOnWheel = false;
            this.tpgAcervos.HorizontalScrollbarSize = 10;
            this.tpgAcervos.Location = new System.Drawing.Point(4, 38);
            this.tpgAcervos.Name = "tpgAcervos";
            this.tpgAcervos.Size = new System.Drawing.Size(649, 418);
            this.tpgAcervos.TabIndex = 1;
            this.tpgAcervos.Text = "Acervos";
            this.tpgAcervos.VerticalScrollbarBarColor = true;
            this.tpgAcervos.VerticalScrollbarHighlightOnWheel = false;
            this.tpgAcervos.VerticalScrollbarSize = 10;
            // 
            // tpgDevolucao
            // 
            this.tpgDevolucao.HorizontalScrollbarBarColor = true;
            this.tpgDevolucao.HorizontalScrollbarHighlightOnWheel = false;
            this.tpgDevolucao.HorizontalScrollbarSize = 10;
            this.tpgDevolucao.Location = new System.Drawing.Point(4, 38);
            this.tpgDevolucao.Name = "tpgDevolucao";
            this.tpgDevolucao.Size = new System.Drawing.Size(649, 418);
            this.tpgDevolucao.TabIndex = 2;
            this.tpgDevolucao.Text = "Devolução";
            this.tpgDevolucao.VerticalScrollbarBarColor = true;
            this.tpgDevolucao.VerticalScrollbarHighlightOnWheel = false;
            this.tpgDevolucao.VerticalScrollbarSize = 10;
            // 
            // tpgEmprestimo
            // 
            this.tpgEmprestimo.HorizontalScrollbarBarColor = true;
            this.tpgEmprestimo.HorizontalScrollbarHighlightOnWheel = false;
            this.tpgEmprestimo.HorizontalScrollbarSize = 10;
            this.tpgEmprestimo.Location = new System.Drawing.Point(4, 38);
            this.tpgEmprestimo.Name = "tpgEmprestimo";
            this.tpgEmprestimo.Size = new System.Drawing.Size(649, 418);
            this.tpgEmprestimo.TabIndex = 3;
            this.tpgEmprestimo.Text = "Emprestimos";
            this.tpgEmprestimo.VerticalScrollbarBarColor = true;
            this.tpgEmprestimo.VerticalScrollbarHighlightOnWheel = false;
            this.tpgEmprestimo.VerticalScrollbarSize = 10;
            // 
            // tpgRelatórios
            // 
            this.tpgRelatórios.HorizontalScrollbarBarColor = true;
            this.tpgRelatórios.HorizontalScrollbarHighlightOnWheel = false;
            this.tpgRelatórios.HorizontalScrollbarSize = 10;
            this.tpgRelatórios.Location = new System.Drawing.Point(4, 38);
            this.tpgRelatórios.Name = "tpgRelatórios";
            this.tpgRelatórios.Size = new System.Drawing.Size(649, 418);
            this.tpgRelatórios.TabIndex = 4;
            this.tpgRelatórios.Text = "Relatórios";
            this.tpgRelatórios.VerticalScrollbarBarColor = true;
            this.tpgRelatórios.VerticalScrollbarHighlightOnWheel = false;
            this.tpgRelatórios.VerticalScrollbarSize = 10;
            // 
            // ElipseForm
            // 
            this.ElipseForm.ElipseRadius = 25;
            this.ElipseForm.TargetControl = this;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(930, 562);
            this.Controls.Add(this.tblMenu);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tblMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Label lblTitulo;
        private MetroFramework.Controls.MetroTabControl tblMenu;
        private MetroFramework.Controls.MetroTabPage tpgHome;
        private MetroFramework.Controls.MetroTabPage tpgAcervos;
        private MetroFramework.Controls.MetroTabPage tpgDevolucao;
        private MetroFramework.Controls.MetroTabPage tpgEmprestimo;
        private MetroFramework.Controls.MetroTabPage tpgRelatórios;
        private Bunifu.Framework.UI.BunifuElipse ElipseForm;
    }
}

