﻿using Biblioteca._User_Controls.Bibliotecario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Forms
{
    public partial class frmLogin : Form
    {
        #region Animations and Fuctions
        Timer timer1 = new Timer();
        Timer timer2 = new Timer();
        Label label = new Label();

        public frmLogin()
        {
            InitializeComponent();

            this.Size = new Size(320, 480);
            imgPassword.Image = Properties.Resources.Password;
            imgPassword.Enabled = false;

            //Variável verificadora
            ativo = true;            
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            timer1.Tick += new EventHandler(Timer1);
            timer2.Tick += new EventHandler(Timer2);

            timer1.Interval = 10;
            timer2.Interval = 5;
            timer1.Start();
        }

        void Timer1 (object sender, EventArgs e)
        {
            LogoPosition();
        }

        void Timer2 (object sender, EventArgs e)
        {
            if (label.Text == "Esqueceu a Senha?")
            {
                Forget();
            }
            else if (label.Text == "Login")
            {
                Login();
            }
        }

        int logo = 1;
        void LogoPosition ()
        {
            if (panel1.Left == 0)
            {
                imgLogo.Top += logo;
                if (imgLogo.Top > 50)
                {
                    timer1.Stop();
                }
            }
            if (panel2.Left == 0)
            {
                imgLogo.Top -= logo;
                if (imgLogo.Top < 34)
                {
                    timer1.Stop();
                }
            }
        }

        void Line ()
        {
            if (panel1.Left == 0)
            {
                Separator2.Size = new Size(104, 3);
                Separator2.BackColor = Color.FromArgb(0, 82, 204);

                Separator1.Size = new Size(105, 2);
                Separator1.BackColor = Color.Silver;
            }
            if (panel2.Left == 0)
            {
                Separator1.Size = new Size(105, 3);
                Separator1.BackColor = Color.FromArgb(0, 82, 204);

                Separator2.Size = new Size(105, 2);
                Separator2.BackColor = Color.Silver;
            }
        }

        int speed = 20;
        void Forget()
        {
            if (panel2.Left > 0)
            {
                timer1.Start();
                Line();

                panel1.Left -= speed;
                panel2.Left -= speed;

                if (panel2.Left == 0)
                {
                    timer2.Stop();
                }
            }
        }

        void Login()
        {
            if (panel1.Left < 0)
            {
                timer1.Start();
                Line();

                panel2.Left += speed;
                panel1.Left += speed;

                if (panel1.Left == 0)
                {
                    timer2.Stop();
                }
            }
        }

        bool ativo;
        void Unlock ()
        {
            if (ativo == true)
            {
                //Destranca
                txtPassword.UseSystemPasswordChar = false;
                imgPassword.Image = Properties.Resources.Unlocked;
                ativo = false;
            }
            else
            {
                //Tranca
                txtPassword.UseSystemPasswordChar = true;
                imgPassword.Image = Properties.Resources.Password;
                ativo = true;
            }
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            if (txtUserName.Text == "User Name")
            {
                txtUserName.Text = string.Empty;
                lnUserName.BackColor = Color.FromArgb(0, 82, 204);
            }
        }

        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text == string.Empty)
            {
                txtUserName.Text = "User Name";
                lnUserName.BackColor = Color.FromArgb(100, 192, 192, 192);
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            if (txtPassword.Text == "Password")
            {
                txtPassword.UseSystemPasswordChar = true;
                txtPassword.Text = string.Empty;
                imgPassword.Enabled = true;
                lnPassword.BackColor = Color.FromArgb(0, 82, 204);
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == string.Empty)
            {
                txtPassword.UseSystemPasswordChar = false;
                txtPassword.Text = "Password";
                imgPassword.Enabled = false;
                lnPassword.BackColor = Color.FromArgb(100, 192, 192, 192);
            }
        }

        private void txtFullName_Enter(object sender, EventArgs e)
        {
            if (txtFullName.Text == "Full Name")
            {
                txtFullName.Text = string.Empty;
            }
        }

        private void txtFullName_Leave(object sender, EventArgs e)
        {
            if (txtFullName.Text == string.Empty)
            {
                txtFullName.Text = "Full Name";
            }
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            if (txtUser.Text == "User Name")
            {
                txtUser.Text = string.Empty;
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            if (txtUser.Text == string.Empty)
            {
                txtUser.Text = "User Name";
            }
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "Password")
            {
                txtPass.UseSystemPasswordChar = true;
                txtPass.Text = string.Empty;
            }
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == string.Empty)
            {
                txtPass.UseSystemPasswordChar = false;
                txtPass.Text = "Password";
            }
        }

        private void txtRepass_Enter(object sender, EventArgs e)
        {
            if (txtRepass.Text == "Re-Password")
            {
                txtRepass.UseSystemPasswordChar = true;
                txtRepass.Text = string.Empty;
            }
        }

        private void txtRepass_Leave(object sender, EventArgs e)
        {
            if (txtRepass.Text == string.Empty)
            {
                txtRepass.UseSystemPasswordChar = false;
                txtRepass.Text = "Re-Password";
            }
        }

        private void Forget(object sender, EventArgs e)
        {
            Label controle = (Label)sender;

            label = controle;
            timer2.Start();
        }

        private void Lock(object sender, EventArgs e)
        {
            Unlock();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion
        private void btnLogin_Click(object sender, EventArgs e)
        {
            
        }

    }
}
