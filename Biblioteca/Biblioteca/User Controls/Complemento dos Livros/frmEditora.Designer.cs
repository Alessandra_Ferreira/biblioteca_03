﻿namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    partial class frmEditora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditora));
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tblAutor = new MetroFramework.Controls.MetroTabControl();
            this.pgRegistro = new MetroFramework.Controls.MetroTabPage();
            this.btnLiterario = new System.Windows.Forms.Button();
            this.btnObra = new System.Windows.Forms.Button();
            this.btnHistoria = new System.Windows.Forms.Button();
            this.pnLiterario = new System.Windows.Forms.Panel();
            this.txtLiterario = new System.Windows.Forms.RichTextBox();
            this.pnObra = new System.Windows.Forms.Panel();
            this.txtObras = new System.Windows.Forms.RichTextBox();
            this.pnHistoria = new System.Windows.Forms.Panel();
            this.txtHistoria = new System.Windows.Forms.RichTextBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnregistrar = new System.Windows.Forms.Button();
            this.txttitulo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pgPesquisar = new MetroFramework.Controls.MetroTabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nvtExemplar = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.TimeHiddenPanel = new System.Windows.Forms.Timer(this.components);
            this.TimeShowPanel = new System.Windows.Forms.Timer(this.components);
            this.TimeOcultar = new System.Windows.Forms.Timer(this.components);
            this.TimeMostrar = new System.Windows.Forms.Timer(this.components);
            this.TimeCorinthians = new System.Windows.Forms.Timer(this.components);
            this.TimePalmeiras = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tblAutor.SuspendLayout();
            this.pgRegistro.SuspendLayout();
            this.pnLiterario.SuspendLayout();
            this.pnObra.SuspendLayout();
            this.pnHistoria.SuspendLayout();
            this.pgPesquisar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).BeginInit();
            this.nvtExemplar.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel5.Location = new System.Drawing.Point(0, 10);
            this.panel5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 663);
            this.panel5.TabIndex = 105;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel3.Location = new System.Drawing.Point(596, 10);
            this.panel3.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 663);
            this.panel3.TabIndex = 103;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(606, 10);
            this.panel2.TabIndex = 104;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Location = new System.Drawing.Point(0, 673);
            this.panel4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(606, 10);
            this.panel4.TabIndex = 102;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label9.Location = new System.Drawing.Point(27, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(430, 37);
            this.label9.TabIndex = 112;
            this.label9.Text = "Gerenciamento de Editoras";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 105);
            this.panel1.TabIndex = 124;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(457, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 122;
            this.pictureBox1.TabStop = false;
            // 
            // tblAutor
            // 
            this.tblAutor.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tblAutor.Controls.Add(this.pgRegistro);
            this.tblAutor.Controls.Add(this.pgPesquisar);
            this.tblAutor.Cursor = System.Windows.Forms.Cursors.Default;
            this.tblAutor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblAutor.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.tblAutor.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.tblAutor.ItemSize = new System.Drawing.Size(90, 45);
            this.tblAutor.Location = new System.Drawing.Point(10, 115);
            this.tblAutor.Name = "tblAutor";
            this.tblAutor.SelectedIndex = 0;
            this.tblAutor.Size = new System.Drawing.Size(586, 558);
            this.tblAutor.TabIndex = 126;
            this.tblAutor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tblAutor.UseSelectable = true;
            // 
            // pgRegistro
            // 
            this.pgRegistro.BackColor = System.Drawing.Color.White;
            this.pgRegistro.Controls.Add(this.btnLiterario);
            this.pgRegistro.Controls.Add(this.btnObra);
            this.pgRegistro.Controls.Add(this.btnHistoria);
            this.pgRegistro.Controls.Add(this.pnLiterario);
            this.pgRegistro.Controls.Add(this.pnObra);
            this.pgRegistro.Controls.Add(this.pnHistoria);
            this.pgRegistro.Controls.Add(this.btnVoltar);
            this.pgRegistro.Controls.Add(this.textBox1);
            this.pgRegistro.Controls.Add(this.label3);
            this.pgRegistro.Controls.Add(this.btnregistrar);
            this.pgRegistro.Controls.Add(this.txttitulo);
            this.pgRegistro.Controls.Add(this.label2);
            this.pgRegistro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pgRegistro.HorizontalScrollbarBarColor = true;
            this.pgRegistro.HorizontalScrollbarHighlightOnWheel = false;
            this.pgRegistro.HorizontalScrollbarSize = 2;
            this.pgRegistro.Location = new System.Drawing.Point(4, 49);
            this.pgRegistro.Name = "pgRegistro";
            this.pgRegistro.Size = new System.Drawing.Size(578, 505);
            this.pgRegistro.TabIndex = 2;
            this.pgRegistro.Text = "Registrar novas Editoras";
            this.pgRegistro.VerticalScrollbarBarColor = true;
            this.pgRegistro.VerticalScrollbarHighlightOnWheel = false;
            this.pgRegistro.VerticalScrollbarSize = 2;
            // 
            // btnLiterario
            // 
            this.btnLiterario.BackColor = System.Drawing.Color.White;
            this.btnLiterario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLiterario.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnLiterario.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnLiterario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnLiterario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLiterario.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLiterario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnLiterario.Image = ((System.Drawing.Image)(resources.GetObject("btnLiterario.Image")));
            this.btnLiterario.Location = new System.Drawing.Point(48, 288);
            this.btnLiterario.Name = "btnLiterario";
            this.btnLiterario.Size = new System.Drawing.Size(489, 39);
            this.btnLiterario.TabIndex = 140;
            this.btnLiterario.Text = "                &Estilo Literário";
            this.btnLiterario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLiterario.UseVisualStyleBackColor = false;
            this.btnLiterario.Click += new System.EventHandler(this.btnLiterario_Click);
            // 
            // btnObra
            // 
            this.btnObra.BackColor = System.Drawing.Color.White;
            this.btnObra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnObra.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnObra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnObra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnObra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObra.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnObra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnObra.Image = ((System.Drawing.Image)(resources.GetObject("btnObra.Image")));
            this.btnObra.Location = new System.Drawing.Point(48, 235);
            this.btnObra.Name = "btnObra";
            this.btnObra.Size = new System.Drawing.Size(489, 39);
            this.btnObra.TabIndex = 140;
            this.btnObra.Text = "            &Obras Famosas";
            this.btnObra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnObra.UseVisualStyleBackColor = false;
            this.btnObra.Click += new System.EventHandler(this.btnObra_Click);
            // 
            // btnHistoria
            // 
            this.btnHistoria.BackColor = System.Drawing.Color.White;
            this.btnHistoria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHistoria.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnHistoria.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnHistoria.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnHistoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHistoria.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnHistoria.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoria.Image")));
            this.btnHistoria.Location = new System.Drawing.Point(48, 182);
            this.btnHistoria.Name = "btnHistoria";
            this.btnHistoria.Size = new System.Drawing.Size(489, 39);
            this.btnHistoria.TabIndex = 140;
            this.btnHistoria.Text = "                         &História";
            this.btnHistoria.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHistoria.UseVisualStyleBackColor = false;
            this.btnHistoria.Click += new System.EventHandler(this.btnHistória_Click);
            // 
            // pnLiterario
            // 
            this.pnLiterario.BackColor = System.Drawing.Color.White;
            this.pnLiterario.Controls.Add(this.txtLiterario);
            this.pnLiterario.Location = new System.Drawing.Point(48, 328);
            this.pnLiterario.Name = "pnLiterario";
            this.pnLiterario.Size = new System.Drawing.Size(489, 2);
            this.pnLiterario.TabIndex = 134;
            // 
            // txtLiterario
            // 
            this.txtLiterario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtLiterario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLiterario.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLiterario.Location = new System.Drawing.Point(0, 0);
            this.txtLiterario.Name = "txtLiterario";
            this.txtLiterario.Size = new System.Drawing.Size(489, 2);
            this.txtLiterario.TabIndex = 0;
            this.txtLiterario.Text = "";
            // 
            // pnObra
            // 
            this.pnObra.BackColor = System.Drawing.Color.White;
            this.pnObra.Controls.Add(this.txtObras);
            this.pnObra.Location = new System.Drawing.Point(48, 276);
            this.pnObra.Name = "pnObra";
            this.pnObra.Size = new System.Drawing.Size(489, 2);
            this.pnObra.TabIndex = 134;
            // 
            // txtObras
            // 
            this.txtObras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtObras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObras.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObras.Location = new System.Drawing.Point(0, 0);
            this.txtObras.Name = "txtObras";
            this.txtObras.Size = new System.Drawing.Size(489, 2);
            this.txtObras.TabIndex = 0;
            this.txtObras.Text = "";
            // 
            // pnHistoria
            // 
            this.pnHistoria.BackColor = System.Drawing.Color.White;
            this.pnHistoria.Controls.Add(this.txtHistoria);
            this.pnHistoria.Location = new System.Drawing.Point(48, 224);
            this.pnHistoria.Name = "pnHistoria";
            this.pnHistoria.Size = new System.Drawing.Size(489, 2);
            this.pnHistoria.TabIndex = 134;
            // 
            // txtHistoria
            // 
            this.txtHistoria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtHistoria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtHistoria.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHistoria.Location = new System.Drawing.Point(0, 0);
            this.txtHistoria.Name = "txtHistoria";
            this.txtHistoria.Size = new System.Drawing.Size(489, 2);
            this.txtHistoria.TabIndex = 0;
            this.txtHistoria.Text = "";
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.White;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatAppearance.BorderSize = 0;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltar.Image")));
            this.btnVoltar.Location = new System.Drawing.Point(-5, 468);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(39, 41);
            this.btnVoltar.TabIndex = 132;
            this.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVoltar.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkGray;
            this.textBox1.Location = new System.Drawing.Point(48, 123);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(489, 29);
            this.textBox1.TabIndex = 131;
            this.textBox1.Text = "Digite o sede da Editora";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label3.Location = new System.Drawing.Point(44, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 19);
            this.label3.TabIndex = 129;
            this.label3.Text = "Sede:";
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnregistrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnregistrar.FlatAppearance.BorderSize = 0;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnregistrar.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistrar.ForeColor = System.Drawing.Color.White;
            this.btnregistrar.Image = ((System.Drawing.Image)(resources.GetObject("btnregistrar.Image")));
            this.btnregistrar.Location = new System.Drawing.Point(383, 448);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(154, 57);
            this.btnregistrar.TabIndex = 126;
            this.btnregistrar.Text = "&Registrar";
            this.btnregistrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnregistrar.UseVisualStyleBackColor = false;
            this.btnregistrar.Click += new System.EventHandler(this.btnregistrar_Click);
            // 
            // txttitulo
            // 
            this.txttitulo.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitulo.ForeColor = System.Drawing.Color.DarkGray;
            this.txttitulo.Location = new System.Drawing.Point(48, 57);
            this.txttitulo.Name = "txttitulo";
            this.txttitulo.Size = new System.Drawing.Size(489, 29);
            this.txttitulo.TabIndex = 123;
            this.txttitulo.Text = "Digite o nome da Editora";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label2.Location = new System.Drawing.Point(44, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 19);
            this.label2.TabIndex = 122;
            this.label2.Text = "Nome:";
            // 
            // pgPesquisar
            // 
            this.pgPesquisar.Controls.Add(this.dataGridView1);
            this.pgPesquisar.Controls.Add(this.nvtExemplar);
            this.pgPesquisar.HorizontalScrollbarBarColor = true;
            this.pgPesquisar.HorizontalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.HorizontalScrollbarSize = 2;
            this.pgPesquisar.Location = new System.Drawing.Point(4, 49);
            this.pgPesquisar.Name = "pgPesquisar";
            this.pgPesquisar.Size = new System.Drawing.Size(578, 505);
            this.pgPesquisar.TabIndex = 3;
            this.pgPesquisar.Text = "Pesquisar por Editoras";
            this.pgPesquisar.VerticalScrollbarBarColor = true;
            this.pgPesquisar.VerticalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.VerticalScrollbarSize = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 31);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(578, 474);
            this.dataGridView1.TabIndex = 90;
            // 
            // nvtExemplar
            // 
            this.nvtExemplar.AddNewItem = this.bindingNavigatorAddNewItem;
            this.nvtExemplar.CountItem = this.bindingNavigatorCountItem;
            this.nvtExemplar.DeleteItem = this.bindingNavigatorDeleteItem;
            this.nvtExemplar.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.nvtExemplar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButton1});
            this.nvtExemplar.Location = new System.Drawing.Point(0, 0);
            this.nvtExemplar.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.nvtExemplar.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.nvtExemplar.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.nvtExemplar.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.nvtExemplar.Name = "nvtExemplar";
            this.nvtExemplar.PositionItem = this.bindingNavigatorPositionItem;
            this.nvtExemplar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.nvtExemplar.Size = new System.Drawing.Size(578, 31);
            this.nvtExemplar.TabIndex = 89;
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(63, 28);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 31);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 28);
            this.toolStripButton1.Text = "Editar";
            // 
            // TimeHiddenPanel
            // 
            this.TimeHiddenPanel.Tick += new System.EventHandler(this.TimeHiddenPanel_Tick);
            // 
            // TimeShowPanel
            // 
            this.TimeShowPanel.Tick += new System.EventHandler(this.TimeShowPanel_Tick);
            // 
            // TimeOcultar
            // 
            this.TimeOcultar.Tick += new System.EventHandler(this.TimeOcultar_Tick);
            // 
            // TimeMostrar
            // 
            this.TimeMostrar.Tick += new System.EventHandler(this.TimeMostrar_Tick);
            // 
            // TimeCorinthians
            // 
            this.TimeCorinthians.Tick += new System.EventHandler(this.TimeCorinthians_Tick);
            // 
            // TimePalmeiras
            // 
            this.TimePalmeiras.Tick += new System.EventHandler(this.TimePalmeiras_Tick);
            // 
            // frmEditora
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(606, 683);
            this.Controls.Add(this.tblAutor);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "frmEditora";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmEditora";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tblAutor.ResumeLayout(false);
            this.pgRegistro.ResumeLayout(false);
            this.pgRegistro.PerformLayout();
            this.pnLiterario.ResumeLayout(false);
            this.pnObra.ResumeLayout(false);
            this.pnHistoria.ResumeLayout(false);
            this.pgPesquisar.ResumeLayout(false);
            this.pgPesquisar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).EndInit();
            this.nvtExemplar.ResumeLayout(false);
            this.nvtExemplar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTabControl tblAutor;
        private MetroFramework.Controls.MetroTabPage pgRegistro;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.TextBox txttitulo;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTabPage pgPesquisar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingNavigator nvtExemplar;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel pnHistoria;
        private System.Windows.Forms.Timer TimeHiddenPanel;
        private System.Windows.Forms.Button btnHistoria;
        private System.Windows.Forms.RichTextBox txtHistoria;
        private System.Windows.Forms.Button btnObra;
        private System.Windows.Forms.Timer TimeShowPanel;
        private System.Windows.Forms.Panel pnObra;
        private System.Windows.Forms.RichTextBox txtObras;
        private System.Windows.Forms.Button btnLiterario;
        private System.Windows.Forms.Timer TimeOcultar;
        private System.Windows.Forms.Timer TimeMostrar;
        private System.Windows.Forms.Panel pnLiterario;
        private System.Windows.Forms.RichTextBox txtLiterario;
        private System.Windows.Forms.Timer TimeCorinthians;
        private System.Windows.Forms.Timer TimePalmeiras;
    }
}