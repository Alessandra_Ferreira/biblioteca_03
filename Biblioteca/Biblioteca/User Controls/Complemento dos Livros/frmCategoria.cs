﻿using Biblioteca.Utilitarios.Notificacoes;
using Biblioteca.Validacoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    public partial class frmCategoria : Form
    {
        #region animacoes_InitializeComponent
        bool exemplo;
        public frmCategoria()
        {
            InitializeComponent();
            exemplo = false;
            pnExemplos.Visible = false;
        }

        private void btnExemplo_Click(object sender, EventArgs e)
        {
            if (exemplo == false)
            {
                TimerExemploHidden.Start();
            }
            else
            {
                TimerExemploShow.Start();
            }

        }

        int tamanhoExemplo = 2;
        private void TimerExemploHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoExemplo > 185)
            {
                TimerExemploHidden.Stop();
                exemplo = true;
                this.Refresh();
            }
            else
            {
                pnExemplos.Visible = true;
                pnExemplos.Size = new Size(pnExemplos.Size.Width, tamanhoExemplo);
                tamanhoExemplo += 10;
            }
        }

        private void TimerExemploShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoExemplo < 2)
            {
                TimerExemploShow.Stop();
                exemplo = false;
                this.Refresh();
                pnExemplos.Visible = false;
            }
            else
            {
                pnExemplos.Size = new Size(pnExemplos.Size.Width, tamanhoExemplo);
                tamanhoExemplo -= 10;
            }
        }
        #endregion

        Notificacoes notificacoesMsg = new Notificacoes();
        private void ValidarNomes()
        {
            VerificarNome verificacaodeNome = new VerificarNome();
            bool nome = verificacaodeNome.VNome(txtNome.Text);
            string msg = nome == false
            ? "Nome invalido"
             : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);

        }
    }

   
}
