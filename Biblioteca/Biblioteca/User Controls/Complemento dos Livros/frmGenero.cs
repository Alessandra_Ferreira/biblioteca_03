﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    public partial class frmGenero : Form
    {
        public frmGenero()
        {
            InitializeComponent();
            pnExemplos.Visible = false;
        }

        private void btnExemplo_Enter(object sender, EventArgs e)
        {
            TimerGenreHidden.Start();
        }

        int genreHeight = 2;
        private void TimerGenreHidden_Tick(object sender, EventArgs e)
        {
            if (genreHeight > 133)
            {
                TimerGenreHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnExemplos.Visible = true;
                pnExemplos.Size = new Size(pnExemplos.Size.Width, genreHeight);
                genreHeight += 10;
            }

        }

        private void pnExemplos_Leave(object sender, EventArgs e)
        {
            TimerGenreShow.Start();
        }

        private void TimerGenreShow_Tick(object sender, EventArgs e)
        {
            if (genreHeight < 2)
            {
                TimerGenreShow.Stop();
                this.Refresh();
                pnExemplos.Visible = false;
            }
            else
            {
                pnExemplos.Size = new Size(pnExemplos.Size.Width, genreHeight);
                genreHeight -= 10;
            }
        }
    }
}
