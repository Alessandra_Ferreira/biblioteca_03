﻿using Biblioteca.Utilitarios.Notificacoes;
using Biblioteca.Validacoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    public partial class frmAutor : Form
    {

        #region animacoes_e_inicializeComponent 
        public frmAutor()
        {
            InitializeComponent();
 
            //3º Deixe os Panels com a visibilidade para FALSE, para que ele não mostre para o Usuário
            pnFrases.Visible = false;
            pnObra.Visible = false;
            pnEscrita.Visible = false;

        }

        int tamanhoFrase = 2;
        private void btnFrases_Enter(object sender, EventArgs e)
        {
            TimerFrasesHidden.Start();
        }

        private void TimerFrasesHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoFrase > 110)
            {
                TimerFrasesHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnFrases.Visible = true;
                pnFrases.Size = new Size(pnFrases.Size.Width, tamanhoFrase);
                tamanhoFrase += 10;
                btnObra.Location = new Point(48, 348);
                btnEscrita.Location = new Point(48, 401);

            }
        }

        private void pnFrases_Leave(object sender, EventArgs e)
        {
            TimerFrasesShow.Start();
        }

        private void TimerFrasesShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoFrase < 2)
            {
                TimerFrasesShow.Stop();
                this.Refresh();
                pnFrases.Visible = false;
            }
            else
            {
                pnFrases.Size = new Size(pnFrases.Size.Width, tamanhoFrase);
                btnObra.Location = new Point(48, 244);
                btnEscrita.Location = new Point(48, 297);
                tamanhoFrase -= 10;
            }
        }

        private void btnObra_Enter(object sender, EventArgs e)
        {
            TimerObrasHidden.Start();
        }

        int tamanhoObra = 2;
        private void TimerObrasHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoObra > 110)
            {
                TimerObrasHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnObra.Visible = true;
                pnObra.Size = new Size(pnObra.Size.Width, tamanhoObra);
                tamanhoObra += 10;
            }
        }

        private void pnObra_Leave(object sender, EventArgs e)
        {
            TimerObrasShow.Start();
        }

        private void TimerObrasShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoObra < 2)
            {
                TimerObrasShow.Stop();
                this.Refresh();
                pnObra.Visible = false;
            }
            else
            {
                pnObra.Size = new Size(pnObra.Size.Width, tamanhoObra);
                tamanhoObra -= 10;
            }
        }

        private void btnEscrita_Enter(object sender, EventArgs e)
        {
            TimerStyleHidden.Start();
        }

        int tamanhoStyle = 2;
        private void TimerStyleHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoStyle > 110)
            {
                TimerStyleHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnEscrita.Visible = true;
                pnEscrita.Size = new Size(pnEscrita.Size.Width, tamanhoStyle);
                tamanhoStyle += 10;
            }
        }

        private void pnEscrita_Leave(object sender, EventArgs e)
        {
            TimerStyleShow.Start();
        }

        private void TimerStyleShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoStyle < 2)
            {
                TimerStyleShow.Stop();
                this.Refresh();
                pnEscrita.Visible = false;
            }
            else
            {
                pnEscrita.Size = new Size(pnEscrita.Size.Width, tamanhoStyle);
                tamanhoStyle -= 10;
            }
        }

        #endregion
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            
        }

        Notificacoes notificacoesMsg = new Notificacoes();

        private void ValidarNomes()
        {
            VerificarNome verificacaodeNome = new VerificarNome();
            bool nome = verificacaodeNome.VNome(txttitulo.Text);         
            string msg = nome == false
            ? "Nome invalido"
             :"";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);
            bool pseudonimo = verificacaodeNome.VNome(txttitulo.Text);         
            string msgP = pseudonimo == false
            ? "Pseudonimo invalido"
             : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msgP);

        }
        private void verificarDatas()
        {
            VerificarData verificacaodeData = new VerificarData();
            bool data = verificacaodeData.VData(txtData.Text);
            string msg =data== false
           ? "Formato de Data Invalido"
            : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);

        }

    }
}
