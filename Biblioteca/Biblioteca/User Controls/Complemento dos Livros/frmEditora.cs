﻿using Biblioteca.Utilitarios.Notificacoes;
using Biblioteca.Validacoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    public partial class frmEditora : Form
    {
        #region animacoes_InitializeComponent
        bool hidden;
        bool oculto;
        bool esconder;

        public frmEditora()
        {
            InitializeComponent();
            pnHistoria.Visible = false;
            pnObra.Visible = false;
            pnLiterario.Visible = false;

            hidden = false;                  
            oculto = false;                  
            esconder = false;                
        }

        int panelHistoria = 2; 
        private void TimeHiddenPanel_Tick(object sender, EventArgs e)
        {
            if (panelHistoria > 110) 
            {
                TimeHiddenPanel.Stop(); 
                hidden = true;
                this.Refresh();
            }
            else
            {
                pnHistoria.Visible = true;
                this.btnObra.Location = new Point(this.btnObra.Location.X, this.btnObra.Location.Y + 10); 
                this.btnLiterario.Location = new Point(this.btnLiterario.Location.X, this.btnLiterario.Location.Y + 10);
                this.pnHistoria.Size = new Size(this.pnHistoria.Size.Width, panelHistoria); 
                panelHistoria += 11;
            }
        }

        private void TimeShowPanel_Tick(object sender, EventArgs e)
        {
            if (panelHistoria < 2)
            {
                TimeShowPanel.Stop();
                hidden = false;
                this.Refresh();
            }
            else
            {
                pnHistoria.Visible = false;
                this.btnObra.Location = new Point(this.btnObra.Location.X, this.btnObra.Location.Y - 10);
                this.btnLiterario.Location = new Point(this.btnLiterario.Location.X, this.btnLiterario.Location.Y - 10);
                this.pnHistoria.Size = new Size(this.pnHistoria.Size.Width, panelHistoria);
                panelHistoria -= 11;
            }
        }

        int panelObra = 2; 
        private void TimeOcultar_Tick(object sender, EventArgs e)
        {
            if (panelObra > 110)
            {
                TimeOcultar.Stop();
                oculto = true;
                this.Refresh();
            }
            else
            {
                pnObra.Visible = true;
                this.btnLiterario.Location = new Point(this.btnLiterario.Location.X, this.btnLiterario.Location.Y +10);
                this.pnObra.Size = new Size(this.pnObra.Size.Width, panelObra);
                panelObra += 11;
            }

        }

        private void TimeMostrar_Tick(object sender, EventArgs e)
        {
            if (panelObra < 2)
            {
                TimeMostrar.Stop();
                oculto = false;
                this.Refresh();
                
            }
            else
            {
                pnObra.Visible = false;
                this.btnLiterario.Location = new Point(this.btnLiterario.Location.X, this.btnLiterario.Location.Y - 10);
                this.pnObra.Size = new Size(this.pnObra.Size.Width, panelObra);
                panelObra -= 11;
            }
        }

        int panelLiterario = 2; 
        private void TimeCorinthians_Tick(object sender, EventArgs e)
        {
            if (panelLiterario > 110)
            {
                TimeCorinthians.Stop();
                esconder = true;
                this.Refresh();
            }
            else
            {
                pnLiterario.Visible = true;
                this.pnLiterario.Size = new Size(this.pnLiterario.Size.Width, panelLiterario);
                panelLiterario += 15;
            }
        }

        private void TimePalmeiras_Tick(object sender, EventArgs e)
        {
            if (panelLiterario < 2)
            {
                TimePalmeiras.Stop();
                esconder = false;
                pnLiterario.Visible = false;
                this.Refresh();
            }
            else
            {
                this.pnLiterario.Size = new Size(this.pnLiterario.Size.Width, panelLiterario);
                panelLiterario -= 15;
            }
        }

        private void btnHistória_Click(object sender, EventArgs e)
        {
            if (hidden == false)
            {
                TimeHiddenPanel.Start();
                if (oculto == true || esconder == true)
                {
                    TimeMostrar.Start();
                    TimePalmeiras.Start();
                }
            }
            else
            {
                TimeShowPanel.Start();
                if (oculto == false || esconder == false)
                {
                    TimeMostrar.Start();
                    TimePalmeiras.Start();
                }
            }
        }
        private void btnObra_Click(object sender, EventArgs e)
        {
            if (oculto == false)
            {
                TimeOcultar.Start();

                if (hidden == true || esconder == true)
                {
                    TimeShowPanel.Start();
                    TimePalmeiras.Start();
                }
            }
            else
            {
                TimeMostrar.Start();
                if (hidden == false || esconder == false)
                {
                    TimeShowPanel.Start();
                    TimePalmeiras.Start();
                }
            }
        }

        private void btnLiterario_Click(object sender, EventArgs e)
        {
            if (esconder == false)
            {
                if (hidden == true || oculto == true)
                {
                    TimeShowPanel.Start();
                    TimeMostrar.Start();
                }
                TimeCorinthians.Start();
            }
            else
            {
                if (hidden == false || oculto == false)
                {
                    TimeShowPanel.Start();
                    TimeMostrar.Start();
                }
                TimePalmeiras.Start();
            }
        }
        #endregion

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            
        }
        Notificacoes notificacoesMsg = new Notificacoes();

        private void ValidarNomes()
        {
            VerificarNome verificacaodeNome = new VerificarNome();
            bool nome = verificacaodeNome.VNome(txttitulo.Text);
            string msg = nome == false
            ? "Nome invalido"
             : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);
            
        }
    }
}
