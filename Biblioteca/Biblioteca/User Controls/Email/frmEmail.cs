﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.User_Controls.Email;

namespace Biblioteca._User_Controls.Email
{
    public partial class frmEmail : UserControl
    {
        public frmEmail()
        {
            InitializeComponent();
        }

        private void EnviarEmail()
        {
            frmInformacoesEmail enviar = new frmInformacoesEmail();
            bool html = chkHTML.Checked;
            enviar.InformacoesEmail(txtEmail.text,
                txtAssunto.text, 
                txtDestinatario.text,
                txtMensagem.Text,
                html );
            enviar.Show();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            EnviarEmail();
        }
    }
}
