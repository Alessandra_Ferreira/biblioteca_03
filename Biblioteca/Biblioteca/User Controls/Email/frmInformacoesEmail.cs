﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.Utilitarios.Email;


namespace Biblioteca.User_Controls.Email
{
    public partial class frmInformacoesEmail : Form
    {
        public frmInformacoesEmail()
        {
            InitializeComponent();
        }
        private string para;
        private string assunto;
        private string copia;
        private string mensagem;
        private bool comhtml;

        public void InformacoesEmail(string para, 
                                     string assunto,
                                     string copia,
                                     string mensagem,
                                     bool comhtml)
        {
            para = this.para;
            assunto = this.assunto;
            copia = this.copia;
            mensagem = this.mensagem;
            comhtml = this.comhtml;

        }
        private void EnviarEmail()
        {
            EnvioDeEmail email = new EnvioDeEmail();
            email.EnviarEmail(
                para,
                chkgmail.Checked,
                chkoutlook.Checked,
                txtEmail.text,
                txtsenha.text,
                copia,
                copia,
                assunto,
                mensagem,
                comhtml);
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            EnviarEmail();
        }
    }
}
