﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Son_User_Controls.Complemento_dos_Livros
{
    public partial class frmMidia : Form
    {
        public frmMidia()
        {
            InitializeComponent();
            pnDuracao.Visible = false;
        }

        #region TImerAnimation
        private void btnExemplo_Enter(object sender, EventArgs e)
        {
            TimerDurationHidden.Start();
        }

        int durationHeight = 2;
        private void TimerDurationHidden_Tick(object sender, EventArgs e)
        {
            if (durationHeight > 175)
            {
                TimerDurationHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnDuracao.Visible = true;
                pnDuracao.Size = new Size(pnDuracao.Size.Width, durationHeight);
                durationHeight += 10;
            }
        }

        private void txtDuracao_Leave(object sender, EventArgs e)
        {
            TimerDurationShow.Start();
        }
        

        private void TimerDurationShow_Tick(object sender, EventArgs e)
        {
            if (durationHeight < 2)
            {
                TimerDurationShow.Stop();
                this.Refresh();
                pnDuracao.Visible = false;
            }
            else
            {
                pnDuracao.Size = new Size(pnDuracao.Size.Width, durationHeight);
                durationHeight -= 10;
            }
        }
        #endregion 
    }
}
