﻿using Biblioteca.Utilitarios.Notificacoes;
using Biblioteca.Validacoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    public partial class frmAutor : Form
    {

        #region TimerAnimation 
        //1º Variáveis de escopo. Elas terão a função de nos notificar se os panels estão ativos ou não.
        bool frase;
        bool obra;
        bool estilo;

       
        public frmAutor()
        {
            InitializeComponent();
 
            //2º Seus valores começam como FALSE, pois eles estão em OCULTO
            frase = false;
            obra = false;
            estilo = false;

            //3º Deixe os Panels com a visibilidade para FALSE, para que ele não mostre para o Usuário
            pnFrases.Visible = false;
            pnObra.Visible = false;
            pnEscrita.Visible = false;

        }

        //4º Chame o Timer, bem como a função Start
        private void btnFrases_Click(object sender, EventArgs e)
        {
            if (frase == false)
            {
                if (obra == true || estilo == true) //Se um dos Panels forem pressionados (Panels Ocultos)
                {
                    TimerObrasShow.Start();
                    TimerStyleShow.Start();
                }
                TimerFrasesHidden.Start();  //Esse Timer serve para ocultar os controles 
            }
            else
            {
                if (obra == false || estilo == false) //Se os panel estiverem a amostra
                {
                    TimerObrasShow.Start();
                    TimerStyleShow.Start();
                }
                TimerFrasesShow.Start();   //Esse Timer serve para mostrar os controles ocultos
            }

        }

        //5º É aqui que a mágica acontece!!! Pois este evento é como se fosse uma repetição que necessita ser executada
        //Este Timer, irá mostrar os Panel ocultos
        int tamanhoFrase = 2; //Esse valor dado a váriavel é na verdade a altura do Panel
        private void TimerFrasesHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoFrase > 110) //Valor que o panel deve chegar ao fim dessa condição
            {
                TimerFrasesHidden.Stop(); //Quando chegar nesta condição, o controle deverá parar!
                frase = true;            //Ou seja, o panel agora não está mais Oculto e sim aparente
                this.Refresh();
            }
            else
            {
                pnFrases.Visible = true; //Enquanto estiver na repetição, o Panel fica vísivel para a visualização do User
                pnFrases.Size = new Size(pnFrases.Size.Width, tamanhoFrase); //A VAR de escopo serve para aumentar o tamanho
                btnObra.Location = new Point(btnObra.Location.X, btnObra.Location.Y + 10); //O botão acompanhará a crescente do Panel
                btnEscrita.Location = new Point(btnEscrita.Location.X, btnEscrita.Location.Y + 10); //O botão acompanhará a crescente do Panel
                tamanhoFrase += 10; //Significa que a cada repetição haverá um acrescimo de 10, além de levar cerca de 10 milissegundos para executar essa repetição
            }
        }
        //6º A outra Magia!! Este Timer, irá ocultar os panels
        private void TimerFrasesShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoFrase < 2) //Deverá subtrair o valor do Panel até chegar em 2;
            {
                TimerFrasesShow.Stop(); // Quando chegar nesta condição, o controle deverá parar!
                frase = false;         // Ou seja, o panel agora está oculto
                this.Refresh();
            }
            else
            {
                pnFrases.Visible = false; //O panel retornará para o tamanho padrão e deverá retornar ao estado visível de FALSE
                pnFrases.Size = new Size(pnFrases.Size.Width, tamanhoFrase); //Variável de escopo diminuindo o valor do Panel
                btnObra.Location = new Point(btnObra.Location.X, btnObra.Location.Y - 10); //O valor inverso ao de cima
                btnEscrita.Location = new Point(btnEscrita.Location.X, btnEscrita.Location.Y - 10); //O valor inverso ao de cima
                tamanhoFrase -= 10; // Significa que o cada repetição haverá um descrécimo de 10, aém de levar cerda de 10 milissegundos para executar essa repetição
            }

        }

        private void btnObra_Click(object sender, EventArgs e)
        {
            if (obra == false)
            {
                if (frase == true || estilo == true)
                {
                    TimerFrasesShow.Start();
                    TimerStyleShow.Start();
                }
                TimerObrasHidden.Start();
            }
            else
            {
                if (frase == false || estilo == false)
                {
                    TimerFrasesShow.Start();
                    TimerStyleShow.Start();
                }
                TimerObrasShow.Start();
            }
        }

        int tamanhoObra = 2;
        private void TimerObrasHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoObra > 110)
            {
                TimerObrasHidden.Stop();
                obra = true;
                this.Refresh();
            }
            else
            {
                pnObra.Visible = true;
                pnObra.Size = new Size(pnObra.Size.Width, tamanhoObra);
                btnEscrita.Location = new Point(btnEscrita.Location.X, btnEscrita.Location.Y + 10);
                tamanhoObra += 10;
            }
        }

        private void TimerObrasShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoObra < 2)
            {
                TimerObrasShow.Stop();
                obra = false;
                this.Refresh();
            }
            else
            {
                pnObra.Visible = false;
                pnObra.Size = new Size(pnObra.Size.Width, tamanhoObra);
                btnEscrita.Location = new Point(btnEscrita.Location.X, btnEscrita.Location.Y - 10);
                tamanhoObra -=10;
            }
        }

        private void btnEscrita_Click(object sender, EventArgs e)
        {
            if (estilo == false)
            {
                if (frase == true || obra == true)
                {
                    TimerFrasesShow.Start();
                    TimerObrasShow.Start();
                }
                TimerStyleHidden.Start();
            }
            else
            {
                if (frase == false || obra == false)
                {
                    TimerFrasesShow.Start();
                    TimerObrasShow.Start();
                }
                TimerStyleShow.Start();
            }
        }

        int tamanhoStyle = 2;
        private void TimerStyleHidden_Tick(object sender, EventArgs e)
        {
            if (tamanhoStyle > 110)
            {
                TimerStyleHidden.Stop();
                estilo = true;
                this.Refresh();
            }
            else
            {
                pnEscrita.Visible = true;
                pnEscrita.Size = new Size(pnEscrita.Size. Width, tamanhoStyle);
                btnRegistrar.Location = new Point(btnRegistrar.Location.X, btnRegistrar.Location.Y + 0);    
                tamanhoStyle += 10;
            }
        }

        private void TimerStyleShow_Tick(object sender, EventArgs e)
        {
            if (tamanhoStyle < 2)
            {
                TimerStyleShow.Stop();
                estilo = false;
                this.Refresh();
                pnEscrita.Visible = false;
            }
            else
            {
                pnEscrita.Size = new Size(pnEscrita.Size.Width, tamanhoStyle);
                btnRegistrar.Location = new Point(btnRegistrar.Location.X, btnRegistrar.Location.Y - 0);
                tamanhoStyle -= 10;
            }
        }
        #endregion
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            
        }

        Notificacoes notificacoesMsg = new Notificacoes();

        private void ValidarNomes()
        {
            VerificarNome verificacaodeNome = new VerificarNome();
            bool nome = verificacaodeNome.VNome(txttitulo.Text);         
            string msg = nome == false
            ? "Nome invalido"
             :"";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);
            bool pseudonimo = verificacaodeNome.VNome(txttitulo.Text);         
            string msgP = pseudonimo == false
            ? "Pseudonimo invalido"
             : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msgP);

        }
        private void verificarDatas()
        {
            VerificarData verificacaodeData = new VerificarData();
            bool data = verificacaodeData.VData(txtData.Text);
            string msg =data== false
           ? "Formato de Data Invalido"
            : "";
            if (msg != string.Empty)
                notificacoesMsg.NotificaçãoSemImagem(msg);

        }
    }
}
