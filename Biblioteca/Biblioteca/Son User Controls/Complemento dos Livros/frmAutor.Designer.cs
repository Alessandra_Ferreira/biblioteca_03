﻿namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    partial class frmAutor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutor));
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tblAutor = new MetroFramework.Controls.MetroTabControl();
            this.pgRegistro = new MetroFramework.Controls.MetroTabPage();
            this.pnObra = new System.Windows.Forms.Panel();
            this.pnFrases = new System.Windows.Forms.Panel();
            this.txtData = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txttitulo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pgPesquisar = new MetroFramework.Controls.MetroTabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nvtExemplar = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.TimerFrasesHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerFrasesShow = new System.Windows.Forms.Timer(this.components);
            this.TimerObrasHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerObrasShow = new System.Windows.Forms.Timer(this.components);
            this.TimerStyleHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerStyleShow = new System.Windows.Forms.Timer(this.components);
            this.btnObra = new System.Windows.Forms.Button();
            this.btnFrases = new System.Windows.Forms.Button();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtObras = new System.Windows.Forms.RichTextBox();
            this.txtEscrita = new System.Windows.Forms.RichTextBox();
            this.pnEscrita = new System.Windows.Forms.Panel();
            this.btnEscrita = new System.Windows.Forms.Button();
            this.txtFrases = new System.Windows.Forms.RichTextBox();
            this.panel1.SuspendLayout();
            this.tblAutor.SuspendLayout();
            this.pgRegistro.SuspendLayout();
            this.pnObra.SuspendLayout();
            this.pnFrases.SuspendLayout();
            this.pgPesquisar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).BeginInit();
            this.nvtExemplar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnEscrita.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel5.Location = new System.Drawing.Point(0, 10);
            this.panel5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 663);
            this.panel5.TabIndex = 105;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel3.Location = new System.Drawing.Point(596, 10);
            this.panel3.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 663);
            this.panel3.TabIndex = 103;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(606, 10);
            this.panel2.TabIndex = 104;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Location = new System.Drawing.Point(0, 673);
            this.panel4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(606, 10);
            this.panel4.TabIndex = 102;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label9.Location = new System.Drawing.Point(27, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(429, 37);
            this.label9.TabIndex = 112;
            this.label9.Text = "Gerenciamento de Autores";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 105);
            this.panel1.TabIndex = 124;
            // 
            // tblAutor
            // 
            this.tblAutor.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tblAutor.Controls.Add(this.pgRegistro);
            this.tblAutor.Controls.Add(this.pgPesquisar);
            this.tblAutor.Cursor = System.Windows.Forms.Cursors.Default;
            this.tblAutor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblAutor.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.tblAutor.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.tblAutor.ItemSize = new System.Drawing.Size(90, 45);
            this.tblAutor.Location = new System.Drawing.Point(10, 115);
            this.tblAutor.Name = "tblAutor";
            this.tblAutor.SelectedIndex = 0;
            this.tblAutor.Size = new System.Drawing.Size(586, 558);
            this.tblAutor.TabIndex = 126;
            this.tblAutor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tblAutor.UseSelectable = true;
            // 
            // pgRegistro
            // 
            this.pgRegistro.BackColor = System.Drawing.Color.White;
            this.pgRegistro.Controls.Add(this.btnEscrita);
            this.pgRegistro.Controls.Add(this.btnObra);
            this.pgRegistro.Controls.Add(this.btnFrases);
            this.pgRegistro.Controls.Add(this.pnEscrita);
            this.pgRegistro.Controls.Add(this.pnObra);
            this.pgRegistro.Controls.Add(this.pnFrases);
            this.pgRegistro.Controls.Add(this.btnVoltar);
            this.pgRegistro.Controls.Add(this.txtData);
            this.pgRegistro.Controls.Add(this.textBox1);
            this.pgRegistro.Controls.Add(this.label3);
            this.pgRegistro.Controls.Add(this.label1);
            this.pgRegistro.Controls.Add(this.btnRegistrar);
            this.pgRegistro.Controls.Add(this.txttitulo);
            this.pgRegistro.Controls.Add(this.label2);
            this.pgRegistro.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgRegistro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pgRegistro.HorizontalScrollbarBarColor = true;
            this.pgRegistro.HorizontalScrollbarHighlightOnWheel = false;
            this.pgRegistro.HorizontalScrollbarSize = 1;
            this.pgRegistro.Location = new System.Drawing.Point(4, 49);
            this.pgRegistro.Name = "pgRegistro";
            this.pgRegistro.Size = new System.Drawing.Size(578, 505);
            this.pgRegistro.TabIndex = 2;
            this.pgRegistro.Text = "Registrar novos Autores";
            this.pgRegistro.VerticalScrollbarBarColor = true;
            this.pgRegistro.VerticalScrollbarHighlightOnWheel = false;
            this.pgRegistro.VerticalScrollbarSize = 1;
            // 
            // pnObra
            // 
            this.pnObra.BackColor = System.Drawing.Color.White;
            this.pnObra.Controls.Add(this.txtObras);
            this.pnObra.ForeColor = System.Drawing.Color.Black;
            this.pnObra.Location = new System.Drawing.Point(48, 285);
            this.pnObra.Name = "pnObra";
            this.pnObra.Size = new System.Drawing.Size(489, 2);
            this.pnObra.TabIndex = 142;
            // 
            // pnFrases
            // 
            this.pnFrases.BackColor = System.Drawing.Color.White;
            this.pnFrases.Controls.Add(this.txtFrases);
            this.pnFrases.ForeColor = System.Drawing.Color.Black;
            this.pnFrases.Location = new System.Drawing.Point(48, 233);
            this.pnFrases.Name = "pnFrases";
            this.pnFrases.Size = new System.Drawing.Size(489, 2);
            this.pnFrases.TabIndex = 143;
            // 
            // txtData
            // 
            this.txtData.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.ForeColor = System.Drawing.Color.DarkGray;
            this.txtData.Location = new System.Drawing.Point(48, 150);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(489, 29);
            this.txtData.TabIndex = 130;
            this.txtData.Text = "Digite a data de nascimento do Autor";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkGray;
            this.textBox1.Location = new System.Drawing.Point(48, 91);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(489, 29);
            this.textBox1.TabIndex = 131;
            this.textBox1.Text = "Digite o Pseudônimo do Autor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label3.Location = new System.Drawing.Point(44, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 22);
            this.label3.TabIndex = 129;
            this.label3.Text = "Pseudonimo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(44, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 22);
            this.label1.TabIndex = 128;
            this.label1.Text = "Data de Nascimento:";
            // 
            // txttitulo
            // 
            this.txttitulo.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitulo.ForeColor = System.Drawing.Color.DarkGray;
            this.txttitulo.Location = new System.Drawing.Point(48, 32);
            this.txttitulo.Name = "txttitulo";
            this.txttitulo.Size = new System.Drawing.Size(489, 29);
            this.txttitulo.TabIndex = 123;
            this.txttitulo.Text = "Digite o nome do Autor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label2.Location = new System.Drawing.Point(44, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 22);
            this.label2.TabIndex = 122;
            this.label2.Text = "Nome:";
            // 
            // pgPesquisar
            // 
            this.pgPesquisar.Controls.Add(this.dataGridView1);
            this.pgPesquisar.Controls.Add(this.nvtExemplar);
            this.pgPesquisar.HorizontalScrollbarBarColor = true;
            this.pgPesquisar.HorizontalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.HorizontalScrollbarSize = 1;
            this.pgPesquisar.Location = new System.Drawing.Point(4, 49);
            this.pgPesquisar.Name = "pgPesquisar";
            this.pgPesquisar.Size = new System.Drawing.Size(578, 505);
            this.pgPesquisar.TabIndex = 3;
            this.pgPesquisar.Text = "Pesquisar por Autores";
            this.pgPesquisar.VerticalScrollbarBarColor = true;
            this.pgPesquisar.VerticalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.VerticalScrollbarSize = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 31);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(578, 474);
            this.dataGridView1.TabIndex = 90;
            // 
            // nvtExemplar
            // 
            this.nvtExemplar.AddNewItem = this.bindingNavigatorAddNewItem;
            this.nvtExemplar.CountItem = this.bindingNavigatorCountItem;
            this.nvtExemplar.DeleteItem = this.bindingNavigatorDeleteItem;
            this.nvtExemplar.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.nvtExemplar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButton1});
            this.nvtExemplar.Location = new System.Drawing.Point(0, 0);
            this.nvtExemplar.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.nvtExemplar.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.nvtExemplar.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.nvtExemplar.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.nvtExemplar.Name = "nvtExemplar";
            this.nvtExemplar.PositionItem = this.bindingNavigatorPositionItem;
            this.nvtExemplar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.nvtExemplar.Size = new System.Drawing.Size(578, 31);
            this.nvtExemplar.TabIndex = 89;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(63, 28);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 31);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // TimerFrasesHidden
            // 
            this.TimerFrasesHidden.Tick += new System.EventHandler(this.TimerFrasesHidden_Tick);
            // 
            // TimerFrasesShow
            // 
            this.TimerFrasesShow.Tick += new System.EventHandler(this.TimerFrasesShow_Tick);
            // 
            // TimerObrasHidden
            // 
            this.TimerObrasHidden.Tick += new System.EventHandler(this.TimerObrasHidden_Tick);
            // 
            // TimerObrasShow
            // 
            this.TimerObrasShow.Tick += new System.EventHandler(this.TimerObrasShow_Tick);
            // 
            // TimerStyleHidden
            // 
            this.TimerStyleHidden.Tick += new System.EventHandler(this.TimerStyleHidden_Tick);
            // 
            // TimerStyleShow
            // 
            this.TimerStyleShow.Tick += new System.EventHandler(this.TimerStyleShow_Tick);
            // 
            // btnObra
            // 
            this.btnObra.BackColor = System.Drawing.Color.White;
            this.btnObra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnObra.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnObra.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnObra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnObra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnObra.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.btnObra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnObra.Image = ((System.Drawing.Image)(resources.GetObject("btnObra.Image")));
            this.btnObra.Location = new System.Drawing.Point(48, 244);
            this.btnObra.Name = "btnObra";
            this.btnObra.Size = new System.Drawing.Size(489, 39);
            this.btnObra.TabIndex = 145;
            this.btnObra.Text = "            &Obras Famosas";
            this.btnObra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnObra.UseVisualStyleBackColor = false;
            this.btnObra.Click += new System.EventHandler(this.btnObra_Click);
            // 
            // btnFrases
            // 
            this.btnFrases.BackColor = System.Drawing.Color.White;
            this.btnFrases.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFrases.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnFrases.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnFrases.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnFrases.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFrases.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.btnFrases.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnFrases.Image = ((System.Drawing.Image)(resources.GetObject("btnFrases.Image")));
            this.btnFrases.Location = new System.Drawing.Point(48, 191);
            this.btnFrases.Name = "btnFrases";
            this.btnFrases.Size = new System.Drawing.Size(489, 39);
            this.btnFrases.TabIndex = 146;
            this.btnFrases.Text = "                          &Frases";
            this.btnFrases.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFrases.UseVisualStyleBackColor = false;
            this.btnFrases.Click += new System.EventHandler(this.btnFrases_Click);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.White;
            this.btnVoltar.FlatAppearance.BorderSize = 0;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltar.Image")));
            this.btnVoltar.Location = new System.Drawing.Point(-5, 468);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(39, 41);
            this.btnVoltar.TabIndex = 132;
            this.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVoltar.UseVisualStyleBackColor = false;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnRegistrar.FlatAppearance.BorderSize = 0;
            this.btnRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrar.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.ForeColor = System.Drawing.Color.White;
            this.btnRegistrar.Image = ((System.Drawing.Image)(resources.GetObject("btnRegistrar.Image")));
            this.btnRegistrar.Location = new System.Drawing.Point(383, 448);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(154, 57);
            this.btnRegistrar.TabIndex = 126;
            this.btnRegistrar.Text = "&Registrar";
            this.btnRegistrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRegistrar.UseVisualStyleBackColor = false;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 28);
            this.toolStripButton1.Text = "Editar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(441, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 122;
            this.pictureBox1.TabStop = false;
            // 
            // txtObras
            // 
            this.txtObras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtObras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObras.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObras.Location = new System.Drawing.Point(0, 0);
            this.txtObras.Name = "txtObras";
            this.txtObras.Size = new System.Drawing.Size(489, 2);
            this.txtObras.TabIndex = 0;
            this.txtObras.Text = "";
            // 
            // txtEscrita
            // 
            this.txtEscrita.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtEscrita.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEscrita.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEscrita.Location = new System.Drawing.Point(0, 0);
            this.txtEscrita.Name = "txtEscrita";
            this.txtEscrita.Size = new System.Drawing.Size(489, 2);
            this.txtEscrita.TabIndex = 0;
            this.txtEscrita.Text = "";
            // 
            // pnEscrita
            // 
            this.pnEscrita.BackColor = System.Drawing.Color.White;
            this.pnEscrita.Controls.Add(this.txtEscrita);
            this.pnEscrita.ForeColor = System.Drawing.Color.Black;
            this.pnEscrita.Location = new System.Drawing.Point(48, 337);
            this.pnEscrita.Name = "pnEscrita";
            this.pnEscrita.Size = new System.Drawing.Size(489, 2);
            this.pnEscrita.TabIndex = 141;
            // 
            // btnEscrita
            // 
            this.btnEscrita.BackColor = System.Drawing.Color.White;
            this.btnEscrita.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEscrita.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnEscrita.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnEscrita.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnEscrita.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscrita.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.btnEscrita.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnEscrita.Image = ((System.Drawing.Image)(resources.GetObject("btnEscrita.Image")));
            this.btnEscrita.Location = new System.Drawing.Point(48, 297);
            this.btnEscrita.Name = "btnEscrita";
            this.btnEscrita.Size = new System.Drawing.Size(489, 39);
            this.btnEscrita.TabIndex = 144;
            this.btnEscrita.Text = "                   &Estilo Escrita";
            this.btnEscrita.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEscrita.UseVisualStyleBackColor = false;
            this.btnEscrita.Click += new System.EventHandler(this.btnEscrita_Click);
            // 
            // txtFrases
            // 
            this.txtFrases.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtFrases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFrases.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrases.Location = new System.Drawing.Point(0, 0);
            this.txtFrases.Name = "txtFrases";
            this.txtFrases.Size = new System.Drawing.Size(489, 2);
            this.txtFrases.TabIndex = 0;
            this.txtFrases.Text = "";
            // 
            // frmAutor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(606, 683);
            this.Controls.Add(this.tblAutor);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmAutor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAutor";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tblAutor.ResumeLayout(false);
            this.pgRegistro.ResumeLayout(false);
            this.pgRegistro.PerformLayout();
            this.pnObra.ResumeLayout(false);
            this.pnFrases.ResumeLayout(false);
            this.pgPesquisar.ResumeLayout(false);
            this.pgPesquisar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).EndInit();
            this.nvtExemplar.ResumeLayout(false);
            this.nvtExemplar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnEscrita.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroTabControl tblAutor;
        private MetroFramework.Controls.MetroTabPage pgRegistro;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.TextBox txttitulo;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTabPage pgPesquisar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingNavigator nvtExemplar;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Button btnObra;
        private System.Windows.Forms.Button btnFrases;
        private System.Windows.Forms.Panel pnObra;
        private System.Windows.Forms.Panel pnFrases;
        private System.Windows.Forms.Timer TimerFrasesHidden;
        private System.Windows.Forms.Timer TimerFrasesShow;
        private System.Windows.Forms.Timer TimerObrasHidden;
        private System.Windows.Forms.Timer TimerObrasShow;
        private System.Windows.Forms.Timer TimerStyleHidden;
        private System.Windows.Forms.Timer TimerStyleShow;
        private System.Windows.Forms.Button btnEscrita;
        private System.Windows.Forms.Panel pnEscrita;
        private System.Windows.Forms.RichTextBox txtEscrita;
        private System.Windows.Forms.RichTextBox txtObras;
        private System.Windows.Forms.RichTextBox txtFrases;
    }
}