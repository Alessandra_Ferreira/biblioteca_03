﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Son_User_Controls.Complemento_dos_Livros
{
    public partial class frmIdioma : Form
    {
        public frmIdioma()
        {
            InitializeComponent();
            pnExemplos.Visible = false;
        }
        #region TimerAnimation
        private void btnExemplo_Enter(object sender, EventArgs e)
        {
            TimerLanguageHidden.Start();
        }

        int languageHeight = 2;
        private void TimerLanguageHidden_Tick(object sender, EventArgs e)
        {
            if (languageHeight > 185)
            {
                TimerLanguageHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnExemplos.Visible = true;
                pnExemplos.Size = new Size(pnExemplos.Size.Width, languageHeight);
                languageHeight += 10;
            }
        }

        private void pnExemplos_Leave(object sender, EventArgs e)
        {
            TimerLanguageShow.Start();
        }

        private void TimerLanguageShow_Tick(object sender, EventArgs e)
        {
            if (languageHeight < 2)
            {
                TimerLanguageShow.Stop();
                this.Refresh();
                pnExemplos.Visible = false;
            }
            else
            {
                pnExemplos.Size = new Size(pnExemplos.Size.Width, languageHeight);
                languageHeight -= 10;
            }
        }
        #endregion
    }
}
