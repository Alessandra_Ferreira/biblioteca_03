﻿namespace Biblioteca.User_Controls.Complemento_dos_Livros
{
    partial class frmGenero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGenero));
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tblGenero = new MetroFramework.Controls.MetroTabControl();
            this.pgRegistro = new MetroFramework.Controls.MetroTabPage();
            this.btnExemplo = new System.Windows.Forms.Button();
            this.pnExemplos = new System.Windows.Forms.Panel();
            this.txtExemplo = new System.Windows.Forms.RichTextBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pgPesquisar = new MetroFramework.Controls.MetroTabPage();
            this.dgvCategoria = new System.Windows.Forms.DataGridView();
            this.nvtExemplar = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.TimerGenreHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerGenreShow = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tblGenero.SuspendLayout();
            this.pgRegistro.SuspendLayout();
            this.pnExemplos.SuspendLayout();
            this.pgPesquisar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).BeginInit();
            this.nvtExemplar.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel5.Location = new System.Drawing.Point(0, 10);
            this.panel5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 663);
            this.panel5.TabIndex = 109;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel3.Location = new System.Drawing.Point(596, 10);
            this.panel3.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 663);
            this.panel3.TabIndex = 107;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(606, 10);
            this.panel2.TabIndex = 108;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Location = new System.Drawing.Point(0, 673);
            this.panel4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(606, 10);
            this.panel4.TabIndex = 106;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 105);
            this.panel1.TabIndex = 125;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(460, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 122;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 23F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label9.Location = new System.Drawing.Point(22, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(452, 74);
            this.label9.TabIndex = 123;
            this.label9.Text = "Gerenciamento de Gêneros \r\n                 Textuais";
            // 
            // tblGenero
            // 
            this.tblGenero.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tblGenero.Controls.Add(this.pgRegistro);
            this.tblGenero.Controls.Add(this.pgPesquisar);
            this.tblGenero.Cursor = System.Windows.Forms.Cursors.Default;
            this.tblGenero.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblGenero.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.tblGenero.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.tblGenero.ItemSize = new System.Drawing.Size(90, 45);
            this.tblGenero.Location = new System.Drawing.Point(10, 115);
            this.tblGenero.Name = "tblGenero";
            this.tblGenero.SelectedIndex = 1;
            this.tblGenero.Size = new System.Drawing.Size(586, 558);
            this.tblGenero.TabIndex = 131;
            this.tblGenero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tblGenero.UseSelectable = true;
            // 
            // pgRegistro
            // 
            this.pgRegistro.Controls.Add(this.btnExemplo);
            this.pgRegistro.Controls.Add(this.pnExemplos);
            this.pgRegistro.Controls.Add(this.btnVoltar);
            this.pgRegistro.Controls.Add(this.button1);
            this.pgRegistro.Controls.Add(this.txtNome);
            this.pgRegistro.Controls.Add(this.label1);
            this.pgRegistro.Controls.Add(this.label5);
            this.pgRegistro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pgRegistro.HorizontalScrollbarBarColor = true;
            this.pgRegistro.HorizontalScrollbarHighlightOnWheel = false;
            this.pgRegistro.HorizontalScrollbarSize = 2;
            this.pgRegistro.Location = new System.Drawing.Point(4, 49);
            this.pgRegistro.Name = "pgRegistro";
            this.pgRegistro.Size = new System.Drawing.Size(578, 505);
            this.pgRegistro.TabIndex = 2;
            this.pgRegistro.Text = "Registrar novos Gêneros";
            this.pgRegistro.VerticalScrollbarBarColor = true;
            this.pgRegistro.VerticalScrollbarHighlightOnWheel = false;
            this.pgRegistro.VerticalScrollbarSize = 2;
            // 
            // btnExemplo
            // 
            this.btnExemplo.BackColor = System.Drawing.Color.White;
            this.btnExemplo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExemplo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnExemplo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnExemplo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnExemplo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExemplo.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExemplo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnExemplo.Image = ((System.Drawing.Image)(resources.GetObject("btnExemplo.Image")));
            this.btnExemplo.Location = new System.Drawing.Point(48, 208);
            this.btnExemplo.Name = "btnExemplo";
            this.btnExemplo.Size = new System.Drawing.Size(489, 39);
            this.btnExemplo.TabIndex = 144;
            this.btnExemplo.Text = "                         &Exemplo";
            this.btnExemplo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExemplo.UseVisualStyleBackColor = false;
            this.btnExemplo.Enter += new System.EventHandler(this.btnExemplo_Enter);
            // 
            // pnExemplos
            // 
            this.pnExemplos.BackColor = System.Drawing.Color.White;
            this.pnExemplos.Controls.Add(this.txtExemplo);
            this.pnExemplos.Location = new System.Drawing.Point(48, 251);
            this.pnExemplos.Name = "pnExemplos";
            this.pnExemplos.Size = new System.Drawing.Size(489, 2);
            this.pnExemplos.TabIndex = 142;
            this.pnExemplos.Leave += new System.EventHandler(this.pnExemplos_Leave);
            // 
            // txtExemplo
            // 
            this.txtExemplo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtExemplo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExemplo.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExemplo.Location = new System.Drawing.Point(0, 0);
            this.txtExemplo.Name = "txtExemplo";
            this.txtExemplo.Size = new System.Drawing.Size(489, 2);
            this.txtExemplo.TabIndex = 0;
            this.txtExemplo.Text = "";
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.White;
            this.btnVoltar.FlatAppearance.BorderSize = 0;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltar.Image")));
            this.btnVoltar.Location = new System.Drawing.Point(-5, 468);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(39, 41);
            this.btnVoltar.TabIndex = 132;
            this.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVoltar.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(383, 442);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 57);
            this.button1.TabIndex = 126;
            this.button1.Text = "&Registrar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txtNome
            // 
            this.txtNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.txtNome.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.DarkGray;
            this.txtNome.Location = new System.Drawing.Point(49, 86);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(489, 29);
            this.txtNome.TabIndex = 123;
            this.txtNome.Text = "Digite o tipo do gênero";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Georgia", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(192, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 33);
            this.label1.TabIndex = 122;
            this.label1.Text = "DESCRIÇÃO:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label5.Location = new System.Drawing.Point(45, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 22);
            this.label5.TabIndex = 122;
            this.label5.Text = "Nome:";
            // 
            // pgPesquisar
            // 
            this.pgPesquisar.Controls.Add(this.dgvCategoria);
            this.pgPesquisar.Controls.Add(this.nvtExemplar);
            this.pgPesquisar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgPesquisar.HorizontalScrollbarBarColor = true;
            this.pgPesquisar.HorizontalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.HorizontalScrollbarSize = 2;
            this.pgPesquisar.Location = new System.Drawing.Point(4, 49);
            this.pgPesquisar.Name = "pgPesquisar";
            this.pgPesquisar.Size = new System.Drawing.Size(578, 505);
            this.pgPesquisar.TabIndex = 3;
            this.pgPesquisar.Text = "Pesquisar por Gêneros";
            this.pgPesquisar.VerticalScrollbarBarColor = true;
            this.pgPesquisar.VerticalScrollbarHighlightOnWheel = false;
            this.pgPesquisar.VerticalScrollbarSize = 2;
            // 
            // dgvCategoria
            // 
            this.dgvCategoria.AllowUserToAddRows = false;
            this.dgvCategoria.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvCategoria.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCategoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategoria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCategoria.Location = new System.Drawing.Point(0, 31);
            this.dgvCategoria.Name = "dgvCategoria";
            this.dgvCategoria.Size = new System.Drawing.Size(578, 474);
            this.dgvCategoria.TabIndex = 90;
            // 
            // nvtExemplar
            // 
            this.nvtExemplar.AddNewItem = this.bindingNavigatorAddNewItem;
            this.nvtExemplar.CountItem = this.bindingNavigatorCountItem;
            this.nvtExemplar.DeleteItem = this.bindingNavigatorDeleteItem;
            this.nvtExemplar.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.nvtExemplar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripButton1});
            this.nvtExemplar.Location = new System.Drawing.Point(0, 0);
            this.nvtExemplar.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.nvtExemplar.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.nvtExemplar.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.nvtExemplar.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.nvtExemplar.Name = "nvtExemplar";
            this.nvtExemplar.PositionItem = this.bindingNavigatorPositionItem;
            this.nvtExemplar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.nvtExemplar.Size = new System.Drawing.Size(578, 31);
            this.nvtExemplar.TabIndex = 89;
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(63, 28);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 31);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 28);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 28);
            this.toolStripButton1.Text = "Editar";
            // 
            // TimerGenreHidden
            // 
            this.TimerGenreHidden.Tick += new System.EventHandler(this.TimerGenreHidden_Tick);
            // 
            // TimerGenreShow
            // 
            this.TimerGenreShow.Tick += new System.EventHandler(this.TimerGenreShow_Tick);
            // 
            // frmGenero
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(606, 683);
            this.Controls.Add(this.tblGenero);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmGenero";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmGenero";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tblGenero.ResumeLayout(false);
            this.pgRegistro.ResumeLayout(false);
            this.pgRegistro.PerformLayout();
            this.pnExemplos.ResumeLayout(false);
            this.pgPesquisar.ResumeLayout(false);
            this.pgPesquisar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvtExemplar)).EndInit();
            this.nvtExemplar.ResumeLayout(false);
            this.nvtExemplar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroTabControl tblGenero;
        private MetroFramework.Controls.MetroTabPage pgRegistro;
        private System.Windows.Forms.Button btnExemplo;
        private System.Windows.Forms.Panel pnExemplos;
        private System.Windows.Forms.RichTextBox txtExemplo;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroTabPage pgPesquisar;
        private System.Windows.Forms.DataGridView dgvCategoria;
        private System.Windows.Forms.BindingNavigator nvtExemplar;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Timer TimerGenreHidden;
        private System.Windows.Forms.Timer TimerGenreShow;
    }
}