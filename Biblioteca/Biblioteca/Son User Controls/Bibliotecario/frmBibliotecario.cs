﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.Validacoes;

namespace Biblioteca._User_Controls.Bibliotecario
{
    public partial class frmBibliotecario : UserControl
    {
        public frmBibliotecario()
        {
            InitializeComponent();
        }

        void SalvarDados() {
            /*
            = txtNome.text;
            = txtEmail.text;
            = txtCPF.text;
            = txtCelular;
            */
        }

        private void txtNome_OnTextChange(object sender, EventArgs e)
        {
            VerificarNome verificarNome = new VerificarNome();
             bool nome = verificarNome.VNome(txtNome.text);
            if (nome == false)
                throw new ArgumentException("");
        }

        private void txtCelular_OnTextChange(object sender, EventArgs e)
        {
            try
            {
                VerificarValoresDigitados verificarValoresDigitados = new VerificarValoresDigitados();
                verificarValoresDigitados.contemLetras(txtCelular.text);

            }
            catch (ArgumentException message)
            {
                MessageBox.Show(message.Message);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmConsultarBibliotecario consultarBibliotecario = new frmConsultarBibliotecario();
           
        }
    }
}
