﻿namespace Biblioteca.User_Controls.ComplementosLivro
{
    partial class frmFiltroAtrasos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroAtrasos));
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SearchElipse = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btnregistrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton2 = new MetroFramework.Controls.MetroRadioButton();
            this.pnEspera = new System.Windows.Forms.Panel();
            this.pnDays = new System.Windows.Forms.Panel();
            this.rdn35 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton7 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton6 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton5 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton3 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton4 = new MetroFramework.Controls.MetroRadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnDay = new System.Windows.Forms.Button();
            this.btnEspera = new System.Windows.Forms.Button();
            this.TimerDayHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerDayShow = new System.Windows.Forms.Timer(this.components);
            this.TimerWaitingHidden = new System.Windows.Forms.Timer(this.components);
            this.TimerWaitingShow = new System.Windows.Forms.Timer(this.components);
            this.panel2.SuspendLayout();
            this.pnEspera.SuspendLayout();
            this.pnDays.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel5.Location = new System.Drawing.Point(0, 30);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(9, 224);
            this.panel5.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel3.Location = new System.Drawing.Point(540, 30);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(9, 224);
            this.panel3.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel2.Controls.Add(this.btnExit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(549, 30);
            this.panel2.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(512, 0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(37, 30);
            this.btnExit.TabIndex = 0;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(82)))), ((int)(((byte)(159)))));
            this.panel4.Location = new System.Drawing.Point(0, 254);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(549, 9);
            this.panel4.TabIndex = 2;
            // 
            // SearchElipse
            // 
            this.SearchElipse.ElipseRadius = 15;
            this.SearchElipse.TargetControl = this;
            // 
            // btnregistrar
            // 
            this.btnregistrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnregistrar.FlatAppearance.BorderSize = 0;
            this.btnregistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnregistrar.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnregistrar.ForeColor = System.Drawing.Color.White;
            this.btnregistrar.Image = ((System.Drawing.Image)(resources.GetObject("btnregistrar.Image")));
            this.btnregistrar.Location = new System.Drawing.Point(405, 195);
            this.btnregistrar.Name = "btnregistrar";
            this.btnregistrar.Size = new System.Drawing.Size(130, 38);
            this.btnregistrar.TabIndex = 10;
            this.btnregistrar.Text = "Enviar";
            this.btnregistrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnregistrar.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label2.Location = new System.Drawing.Point(16, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Dias Atrasados:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(17, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Lista de Espera:";
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton1.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton1.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton1.Location = new System.Drawing.Point(12, 11);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(50, 19);
            this.metroRadioButton1.TabIndex = 100;
            this.metroRadioButton1.Text = "Sim";
            this.metroRadioButton1.UseCustomBackColor = true;
            this.metroRadioButton1.UseCustomForeColor = true;
            this.metroRadioButton1.UseSelectable = true;
            // 
            // metroRadioButton2
            // 
            this.metroRadioButton2.AutoSize = true;
            this.metroRadioButton2.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton2.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton2.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton2.Location = new System.Drawing.Point(86, 11);
            this.metroRadioButton2.Name = "metroRadioButton2";
            this.metroRadioButton2.Size = new System.Drawing.Size(53, 19);
            this.metroRadioButton2.TabIndex = 101;
            this.metroRadioButton2.Text = "Não";
            this.metroRadioButton2.UseCustomBackColor = true;
            this.metroRadioButton2.UseCustomForeColor = true;
            this.metroRadioButton2.UseSelectable = true;
            // 
            // pnEspera
            // 
            this.pnEspera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pnEspera.Controls.Add(this.metroRadioButton1);
            this.pnEspera.Controls.Add(this.metroRadioButton2);
            this.pnEspera.Location = new System.Drawing.Point(54, 130);
            this.pnEspera.Name = "pnEspera";
            this.pnEspera.Size = new System.Drawing.Size(152, 35);
            this.pnEspera.TabIndex = 102;
            // 
            // pnDays
            // 
            this.pnDays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pnDays.Controls.Add(this.rdn35);
            this.pnDays.Controls.Add(this.metroRadioButton7);
            this.pnDays.Controls.Add(this.metroRadioButton6);
            this.pnDays.Controls.Add(this.metroRadioButton5);
            this.pnDays.Controls.Add(this.metroRadioButton3);
            this.pnDays.Controls.Add(this.metroRadioButton4);
            this.pnDays.Location = new System.Drawing.Point(54, 70);
            this.pnDays.Name = "pnDays";
            this.pnDays.Size = new System.Drawing.Size(480, 35);
            this.pnDays.TabIndex = 103;
            // 
            // rdn35
            // 
            this.rdn35.AutoSize = true;
            this.rdn35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.rdn35.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.rdn35.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.rdn35.ForeColor = System.Drawing.Color.White;
            this.rdn35.Location = new System.Drawing.Point(353, 11);
            this.rdn35.Name = "rdn35";
            this.rdn35.Size = new System.Drawing.Size(128, 19);
            this.rdn35.TabIndex = 105;
            this.rdn35.Text = "35 ou mais dias";
            this.rdn35.UseCustomBackColor = true;
            this.rdn35.UseCustomForeColor = true;
            this.rdn35.UseSelectable = true;
            // 
            // metroRadioButton7
            // 
            this.metroRadioButton7.AutoSize = true;
            this.metroRadioButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.metroRadioButton7.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton7.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton7.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton7.Location = new System.Drawing.Point(279, 11);
            this.metroRadioButton7.Name = "metroRadioButton7";
            this.metroRadioButton7.Size = new System.Drawing.Size(72, 19);
            this.metroRadioButton7.TabIndex = 104;
            this.metroRadioButton7.Text = "30 dias";
            this.metroRadioButton7.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroRadioButton7.UseCustomBackColor = true;
            this.metroRadioButton7.UseCustomForeColor = true;
            this.metroRadioButton7.UseSelectable = true;
            // 
            // metroRadioButton6
            // 
            this.metroRadioButton6.AutoSize = true;
            this.metroRadioButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.metroRadioButton6.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton6.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton6.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton6.Location = new System.Drawing.Point(205, 11);
            this.metroRadioButton6.Name = "metroRadioButton6";
            this.metroRadioButton6.Size = new System.Drawing.Size(72, 19);
            this.metroRadioButton6.TabIndex = 103;
            this.metroRadioButton6.Text = "15 dias";
            this.metroRadioButton6.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroRadioButton6.UseCustomBackColor = true;
            this.metroRadioButton6.UseCustomForeColor = true;
            this.metroRadioButton6.UseSelectable = true;
            // 
            // metroRadioButton5
            // 
            this.metroRadioButton5.AutoSize = true;
            this.metroRadioButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.metroRadioButton5.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton5.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton5.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton5.Location = new System.Drawing.Point(139, 11);
            this.metroRadioButton5.Name = "metroRadioButton5";
            this.metroRadioButton5.Size = new System.Drawing.Size(64, 19);
            this.metroRadioButton5.TabIndex = 102;
            this.metroRadioButton5.Text = "7 dias";
            this.metroRadioButton5.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroRadioButton5.UseCustomBackColor = true;
            this.metroRadioButton5.UseCustomForeColor = true;
            this.metroRadioButton5.UseSelectable = true;
            // 
            // metroRadioButton3
            // 
            this.metroRadioButton3.AutoSize = true;
            this.metroRadioButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.metroRadioButton3.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton3.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton3.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton3.Location = new System.Drawing.Point(7, 11);
            this.metroRadioButton3.Name = "metroRadioButton3";
            this.metroRadioButton3.Size = new System.Drawing.Size(64, 19);
            this.metroRadioButton3.TabIndex = 100;
            this.metroRadioButton3.Text = "3 dias";
            this.metroRadioButton3.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroRadioButton3.UseCustomBackColor = true;
            this.metroRadioButton3.UseCustomForeColor = true;
            this.metroRadioButton3.UseSelectable = true;
            // 
            // metroRadioButton4
            // 
            this.metroRadioButton4.AutoSize = true;
            this.metroRadioButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.metroRadioButton4.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.metroRadioButton4.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.metroRadioButton4.ForeColor = System.Drawing.Color.White;
            this.metroRadioButton4.Location = new System.Drawing.Point(73, 11);
            this.metroRadioButton4.Name = "metroRadioButton4";
            this.metroRadioButton4.Size = new System.Drawing.Size(64, 19);
            this.metroRadioButton4.TabIndex = 101;
            this.metroRadioButton4.Text = "5 dias";
            this.metroRadioButton4.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroRadioButton4.UseCustomBackColor = true;
            this.metroRadioButton4.UseCustomForeColor = true;
            this.metroRadioButton4.UseSelectable = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.label3.Location = new System.Drawing.Point(16, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Titulo Especifico:";
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.Location = new System.Drawing.Point(21, 202);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(367, 29);
            this.comboBox1.TabIndex = 9;
            // 
            // btnDay
            // 
            this.btnDay.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnDay.FlatAppearance.BorderSize = 0;
            this.btnDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDay.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.btnDay.ForeColor = System.Drawing.Color.White;
            this.btnDay.Image = ((System.Drawing.Image)(resources.GetObject("btnDay.Image")));
            this.btnDay.Location = new System.Drawing.Point(16, 70);
            this.btnDay.Name = "btnDay";
            this.btnDay.Size = new System.Drawing.Size(37, 35);
            this.btnDay.TabIndex = 5;
            this.btnDay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDay.UseVisualStyleBackColor = true;
            this.btnDay.Enter += new System.EventHandler(this.btnDay_Enter);
            this.btnDay.Leave += new System.EventHandler(this.btnDay_Leave);
            // 
            // btnEspera
            // 
            this.btnEspera.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnEspera.FlatAppearance.BorderSize = 0;
            this.btnEspera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEspera.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold);
            this.btnEspera.ForeColor = System.Drawing.Color.White;
            this.btnEspera.Image = ((System.Drawing.Image)(resources.GetObject("btnEspera.Image")));
            this.btnEspera.Location = new System.Drawing.Point(16, 130);
            this.btnEspera.Name = "btnEspera";
            this.btnEspera.Size = new System.Drawing.Size(37, 35);
            this.btnEspera.TabIndex = 7;
            this.btnEspera.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEspera.UseVisualStyleBackColor = true;
            this.btnEspera.Enter += new System.EventHandler(this.btnEspera_Enter);
            this.btnEspera.Leave += new System.EventHandler(this.btnEspera_Leave);
            // 
            // TimerDayHidden
            // 
            this.TimerDayHidden.Tick += new System.EventHandler(this.TimerDayHidden_Tick);
            // 
            // TimerDayShow
            // 
            this.TimerDayShow.Tick += new System.EventHandler(this.TimerDayShow_Tick);
            // 
            // TimerWaitingHidden
            // 
            this.TimerWaitingHidden.Tick += new System.EventHandler(this.TimerWaitingHidden_Tick);
            // 
            // TimerWaitingShow
            // 
            this.TimerWaitingShow.Tick += new System.EventHandler(this.TimerWaitingShow_Tick);
            // 
            // frmFiltroAtrasos
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(549, 263);
            this.Controls.Add(this.btnEspera);
            this.Controls.Add(this.btnDay);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pnDays);
            this.Controls.Add(this.pnEspera);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnregistrar);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFiltroAtrasos";
            this.Text = "frmfiltro_de_atrasos";
            this.panel2.ResumeLayout(false);
            this.pnEspera.ResumeLayout(false);
            this.pnEspera.PerformLayout();
            this.pnDays.ResumeLayout(false);
            this.pnDays.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private Bunifu.Framework.UI.BunifuElipse SearchElipse;
        private System.Windows.Forms.Button btnregistrar;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnDays;
        private MetroFramework.Controls.MetroRadioButton rdn35;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton7;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton6;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton5;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton3;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton4;
        private System.Windows.Forms.Panel pnEspera;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnDay;
        private System.Windows.Forms.Button btnEspera;
        private System.Windows.Forms.Timer TimerDayHidden;
        private System.Windows.Forms.Timer TimerDayShow;
        private System.Windows.Forms.Timer TimerWaitingHidden;
        private System.Windows.Forms.Timer TimerWaitingShow;
    }
}