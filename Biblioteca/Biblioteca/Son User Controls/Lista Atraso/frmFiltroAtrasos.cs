﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.User_Controls.ComplementosLivro
{
    public partial class frmFiltroAtrasos : Form
    {
        #region Animacoes_InicializeComponent
        public frmFiltroAtrasos()
        {
            InitializeComponent();
            pnDays.Visible = false;
            pnEspera.Visible = false;
        }

        
        private void btnDay_Enter(object sender, EventArgs e)
        {
            TimerDayHidden.Start();
        }

        int larguraDay = 10;
        private void TimerDayHidden_Tick(object sender, EventArgs e)
        {
            if (larguraDay > 480)
            {
                TimerDayHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnDays.Visible = true;
                pnDays.Size = new Size(larguraDay, pnDays.Size.Height);
                larguraDay += 10;
            }
        }

        private void btnDay_Leave(object sender, EventArgs e)
        {
            TimerDayShow.Start();
        }

        private void TimerDayShow_Tick(object sender, EventArgs e)
        {
            if (larguraDay < 10)
            {
                TimerDayShow.Stop();
                this.Refresh();
                pnDays.Visible = false; 
            }
            else
            {
                pnDays.Size = new Size(larguraDay, pnDays.Size.Height);
                larguraDay -= 35;
            }
        }

        private void btnEspera_Enter(object sender, EventArgs e)
        {
            TimerWaitingHidden.Start();
        }

        int larguraEspera = 10;
        private void TimerWaitingHidden_Tick(object sender, EventArgs e)
        {
            if (larguraEspera > 152)
            {
                TimerWaitingHidden.Stop();
                this.Refresh();
            }
            else
            {
                pnEspera.Visible = true;
                pnEspera.Size = new Size(larguraEspera, pnEspera.Size.Height);
                larguraEspera += 10;
            }
        }

        private void btnEspera_Leave(object sender, EventArgs e)
        {
            TimerWaitingShow.Start();
        }

        private void TimerWaitingShow_Tick(object sender, EventArgs e)
        {
            if (larguraEspera < 10)
            {
                TimerWaitingShow.Stop();
                this.Refresh();
                pnEspera.Visible = false;
            }
            else
            {
                pnEspera.Size = new Size(larguraEspera, pnEspera.Size.Height);
                larguraEspera -= 10;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion
    }
}
