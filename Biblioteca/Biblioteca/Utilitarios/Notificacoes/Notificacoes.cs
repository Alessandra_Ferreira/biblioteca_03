﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tulpep.NotificationWindow;

namespace Biblioteca.Utilitarios.Notificacoes
{
    class Notificacoes
    {
             
        public void NotificaçãoSemImagem(string mensagem)
        {
            PopupNotifier popup = new PopupNotifier();
          //  popup.Image = Properties.Resources.;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"{mensagem}";
            popup.BodyColor= System.Drawing.Color.White;
            popup.BorderColor = System.Drawing.Color.DarkBlue;
           
            //Show Menssage
            popup.Popup();
            
        }

        public void NotificaçãoPassword(string mensagem)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.Password;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"{mensagem}";
            popup.BodyColor = System.Drawing.Color.White;
            popup.BorderColor = System.Drawing.Color.DarkBlue;
            popup.Popup();

        }

    }
}
