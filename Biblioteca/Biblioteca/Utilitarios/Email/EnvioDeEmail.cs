﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace Biblioteca.Utilitarios.Email
{
    class EnvioDeEmail
    {
        public void EnviarEmail(string para,
                                bool gmail,
                                bool outlook,
                                string de,
                                string senha,
                               string copia,
                               string copiaoculta,
                               string assunto,
                               string mensagem,
                               bool comhtml)
        {
            if (gmail == true)
                Envio(para, de, "smtp.gmail.com", senha, copia, copiaoculta, assunto, mensagem, comhtml);
            else if (outlook == true)
                Envio(para, de, "smtp.live.com", senha, copia, copiaoculta, assunto, mensagem, comhtml);

        }
        private void Envio ( string para,
                             string de, string smtpHost,
                             string senha,
                            string copia,
                            string copiaoculta,
                            string assunto,
                            string mensagem,
                            bool comhtml)
        {
            string origem = de;
            string password = senha;

            MailMessage email = new MailMessage();
            MailAddress emailPara = new MailAddress(para);
            MailAddress emailCopia = new MailAddress(copia);
            MailAddress emailCopiaOculta = new MailAddress(copiaoculta);

            email.From = new MailAddress(origem);
            email.To.Add(emailPara);
            email.CC.Add(emailCopia);
            email.Subject = assunto;
            
            if (comhtml == true)
            { string m = Html( mensagem);
                email.Body = m;
            }
           
            else
                email.Body = mensagem;

            email.IsBodyHtml = comhtml;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = smtpHost;
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(origem, senha);

            smtp.Send(email);

        }

        private string Html(string mensagem)
        {
            //Criar o Html
            string Html =$@" {mensagem}";

            return Html;
        }
    }
}
